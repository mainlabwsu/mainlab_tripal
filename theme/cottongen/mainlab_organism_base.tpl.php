<?php
$node = $variables['node'];
$organism = $variables['node']->organism;

// the comment field is a database text field so we have to expand it so that
// it is included in the organism object
$organism = chado_expand_var($organism,'field','organism.comment');
$organism = chado_expand_var($organism, 'table', 'organismprop');
$properties = $node->organism->organismprop;

$prop = array();
$count_cname = 0;
if ($properties) {
  foreach (is_array($properties)?$properties:array($properties) AS $p) {
    $prop[$p->type_id->name] = $p->value;
    if ($p->type_id->name == 'alias_common') {
      $count_cname ++;
    }
  }
}
$num_seq = chado_query("SELECT count (*) FROM {feature} WHERE organism_id = :organism_id", array(':organism_id' => $organism->organism_id))->fetchField();
$rel = $organism->all_relationships;
$subj_rel = $rel ['subject'];
$fertile = 'N/A';
$sterile = 'N/A';
$incompatible = 'N/A';
if (key_exists('fertile with', $subj_rel)) {
  $fertile_first = $subj_rel['fertile with'][0];
  $fertile_count = mainlab_tripal_count($subj_rel['fertile with']);
  $fertile = $fertile_first->genus . ' ' . $fertile_first->species . ' [<a href="?pane=relationships">view all ' . $fertile_count . '</a>]';
}
if (key_exists('sterile with', $subj_rel)) {
  $sterile_first = $subj_rel['sterile with'][0];
  $sterile_count = mainlab_tripal_count($subj_rel['sterile with']);
  $sterile = $sterile_first->genus . ' ' . $sterile_first->species . ' [<a href="?pane=relationships">view all ' . $sterile_count . '</a>]';
}
if (key_exists('incompatible with', $subj_rel)) {
  $incompatible_first = $subj_rel['incompatible with'][0];
  $incompatible_count = mainlab_tripal_count($subj_rel['incompatible with']);
  $incompatible = $incompatible_first->genus . ' ' . $incompatible_first->species . ' [<a href="?pane=relationships">view all ' . $incompatible_count . '</a>]';
}

$organism = chado_expand_var($organism,'table','library');


// get the references. if only one reference exists then we want to convert
// the object into an array, otherwise the value is an array
$libraries = $organism->library;
if (!$libraries) {
  $libraries = array();
}
elseif (!is_array($libraries)) {
  $libraries = array($libraries);
}
$num_lib = mainlab_tripal_count($libraries);
$display_lib = $num_lib > 0 ? '[<a href="?pane=libraries">view all ' . $num_lib . '</a>]' : 'N/A'; 

$result = db_query("SELECT P.pub_id, uniquename FROM {chado.organism_pub} OP INNER JOIN {chado.pub} P ON P.pub_id = OP.pub_id WHERE organism_id = :organism_id", array(':organism_id' => $organism->organism_id));
$pubs = array();
while ($obj = $result->fetchObject()) {
  $pubs[$obj->pub_id] = $obj->uniquename;
}
$pub = 'N/A';
if (mainlab_tripal_count($pubs) > 0) {
  foreach($pubs AS $pid => $p) {
    $lnk = mainlab_tripal_link_record('pub', $pid);
    if ($lnk) {
      $pub = "<a href=$lnk>" . $p . "</a>";
    }
    else {
      $pub = $p;
    }
    break;
  }
}
$grin_id = db_query("SELECT accession FROM chado.organism_dbxref OD INNER JOIN chado.dbxref X ON OD.dbxref_id = X.dbxref_id INNER JOIN chado.db ON db.db_id = X.db_id WHERE organism_id = :organism_id", array(':organism_id' => $organism->organism_id))->fetchField();
?>
  
  <table id="tripal_organism-table-base" class="tripal_organism-table tripal-table tripal-table-vert" style="width:600px;float:left">
    <tr class="tripal_organism-table-odd-row odd">
      <th style="width:250px;">Species Name</th>
      <td><i><?php print $organism->species; ?></i></td>
    </tr>
    <tr class="tripal_organism-table-even-row even">
      <th>Family</th>
      <td><?php if (key_exists('family', $prop)) {print $prop['family']; } else {print "N/A";}?></td>
    </tr>
    <tr class="tripal_organism-table-odd-row odd">
      <th>Scientific Name</th>
      <td><?php if (key_exists('alias_scientific', $prop)) {print $prop['alias_scientific']; } else {print "N/A";}?></td>
    </tr>
    <tr class="tripal_organism-table-even-row even">
      <th>Synonym</th>
      <td><?php if (key_exists('alias_synonym', $prop)) {print $prop['alias_synonym']; } else {print "N/A";} ?></td>
    </tr>
    <tr class="tripal_organism-table-odd-row odd">
      <th>Common Name</th>
      <td><?php if (key_exists('alias_common', $prop)) {print $prop['alias_common'] . " [<a href=\"?pane=alias\">view all $count_cname</a>]"; } else {print "N/A";} ?></td>
    </tr>
        <tr class="tripal_organism-table-even-row even">
      <th>GRIN Taxonomy</th>
      <td>
        <?php if ($grin_id) {?>
          <a href="https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomydetail?id=<?php print $grin_id?>" target=_new>GRIN</a>
        <?php } else { ?>
          N/A
        <?php }?>
      </td>
    </tr>
    <tr class="tripal_organism-table-odd-row odd">
      <th>Geographic Origin</th>
      <td><?php if (key_exists('geographic_origin', $prop)) {print $prop['geographic_origin']; } else {print "N/A";}?></td>
    </tr>
    <tr class="tripal_organism-table-even-row even">
      <th>Genome Group</th>
      <td><?php if (key_exists('genome_group', $prop)) {print $prop['genome_group']; } else {print "N/A";} ?></td>
    </tr>
        <tr class="tripal_organism-table-odd-row odd">
      <th>Genome</th>
      <td><?php if (key_exists('genome_name', $prop)) {print $prop['genome_name']; } else {print "N/A";} ?></td>
    </tr>
    <tr class="tripal_organism-table-even-row even">
      <th>Haploid Chromosome Number</th>
      <td><?php if (key_exists('haploid_chromosome_number', $prop)) {print $prop['haploid_chromosome_number']; } else {print "N/A";}?></td>
    </tr>
    <tr class="tripal_organism-table-odd-row odd">
      <th>Ploidy</th>
      <td><?php if (key_exists('ploidy', $prop)) {print $prop['ploidy']; } else {print "N/A";}?></td>
    </tr>
    <tr class="tripal_organism-table-even-row even">
      <th>Propagation Method</th>
      <td><?php if (key_exists('propagation_method', $prop)) {print $prop['propagation_method']; } else {print "N/A";}?></td>
    </tr>
    <tr class="tripal_organism-table-odd-row odd">
      <th>Fertile with</th><td id="tripal-organism-fertile-species"><?php print $fertile; ?></td>
    </tr>
    <tr class="tripal_organism-table-even-row even">
      <th>Sterile with</th><td id="tripal-organism-sterile-species"><?php print $sterile; ?></td>
    </tr>
    <tr class="tripal_organism-table-odd-row odd">
      <th>Incompatible with</th><td id="tripal-organism-incompatible-species"><?php print $incompatible; ?></td>
    </tr>
        <tr class="tripal_organism-table-even-row even">
      <th>Germplasm</th><td id="tripal-organism-germplasm">N/A</td>
    </tr>
    <tr class="tripal_organism-table-odd-row odd">
      <th>Library</th><td id="tripal-organism-library"><?php print $display_lib; ?></td>
    </tr>
    <tr class="tripal_organism-table-even-row even">
        <th>Sequence</th>
        <?php 
          if ($num_seq > 0) {
            $seq_link = "[<a href=\"/feature_listing/$organism->genus-$organism->species/_/_\">view all $num_seq </a>]";
          } else {
            $seq_link = "N/A";
          } 
        ?>
      <td id="tripal_stock-table-sequence_value"><?php print $seq_link;?></td>
      </tr>
    <tr class="tripal_organism-table-odd-row odd">
      <th>Publication</th>
      <td><?php print $pub; ?></td>
    </tr>
    <tr class="tripal_organism-table-even-row even">
      <th>Description</th>
      <td><?php if (isset($organism->comment)) {print $organism->comment; } else {print "N/A";}?></td>
    </tr>
<?php
// generate the image tag
$image = '';
$image_url = NULL;
if(!$image_url && isset($node->id)) {
    $fid =   db_select('file_usage', 'fu')
    ->fields('fu', array('fid'))
    ->condition('module', 'file')
    ->condition('type', 'TripalEntity')
    ->condition('id', $node->id)
    ->execute()
    ->fetchField();
    if ($fid) {
        $file = file_load($fid);
        $image_url = file_create_url($file->uri);
    }
}
if (db_table_exists('chado_bio_data_1')) {
  $nid = db_select('chado_bio_data_1', 'bio')
     ->fields('bio', array('nid'))
     ->condition('entity_id', $organism->entity_id)
     ->execute()
     ->fetchField();
  $organism->nid = $nid;
}
// If image not found, try to get image from organism_id (Tripal 1.x)
if (!$image_url) {
    $path =  '/sites/default/files/tripal/tripal_organism/images/';
    $file = $path . $organism->genus . '_' . $organism->species . '.jpg';
    if(!file_exists(getcwd() . $file) && isset($organism->nid)) {
        $file = $path . $organism->nid . ".jpg";
    }
    if(file_exists(getcwd() . $file)) {
        global $base_url;
        $image_url = $base_url . $file;
    }
}

if (!$image_url) {
  $image_url = tripal_get_organism_image_url($organism);
}

if ($image_url) {
    $image = "<img class=\"tripal-organism-img\" src=\"$image_url\" style=\"width:238px;height:auto;\">";
}
?>
  </table>
  <?php if ($image_url) {?>
  <div style="float:right;padding:10px 4px;width:248px;margin-right:-6px"><?php print $image; ?></div>
  <?php } ?>
