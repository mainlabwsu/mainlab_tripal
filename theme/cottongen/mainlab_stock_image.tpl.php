<?php
$stock = $variables['node']->stock;
$images = $stock->images;
if (mainlab_tripal_count($images) > 0) {
    $rows = array();
    foreach ($images AS $img) {
        $link = mainlab_tripal_link_record('eimage', $img->eimage_id);
        $plink = mainlab_tripal_link_record('project', $img->project_id);
        $pj = $img->project ? $img->project : 'n/a';
        if ($plink) {
            $pj = "<a href='$plink'>" . $img->project . "</a>";
        }
        if ($link) {
            $rows [] = array ("<a href='$link' style='white-space:nowrap'>". $img->image_uri . '</a>', $img->legend, $pj);
        }
        else {
            $rows [] = array ($img->image_uri, $img->legend, $pj);
        }
    }
    $header = array ('File Name', 'Legend', 'Dataset');
    $table = array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array(
            'id' => 'tripal_stock-table-image',
        ),
        'sticky' => FALSE,
        'caption' => '',
        'colgroups' => array(),
        'empty' => '',
    );
    print theme_table($table);
} 