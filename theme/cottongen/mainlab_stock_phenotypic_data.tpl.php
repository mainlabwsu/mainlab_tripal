<?php
$stock = $variables['node']->stock;
$phenotypic_data = $stock->phenotypic_data;
$num_phenotypic_data = mainlab_tripal_count($phenotypic_data);
$num_per_page = 25;
$pager = mainlab_tripal_get_pager($num_phenotypic_data, $num_per_page, 'phenotypic_data');
$records = mainlab_tripal_get_stock_phenotype($stock->stock_id, $num_per_page, $pager['page'] - 1);

if ($num_phenotypic_data > 0) {
?>

<?php
// Load the table pager javascript code as we'll need it after the allele table is created.
drupal_add_js(drupal_get_path('module', 'mainlab_tripal') . "/theme/js/mainlab_table_pager.js");
?>

  <div id="tripal_stock-phenotypic_data-box" class="tripal_stock-info-box tripal-info-box">
    <div class="tripal_stock-info-box-title tripal-info-box-title">Phenotypic Data</div>
    <div style="float:left; margin-bottom: 15px;">Total <?php print $num_phenotypic_data;?> trait scores</div>
    <?php
      $dir = 'sites/default/files/tripal/mainlab_tripal/download';
      if (!file_exists($dir)) {
        mkdir ($dir, 0777, TRUE);
      }
      $download = $dir . '/phenotypic_data_stock_id_' . $stock->stock_id . '.csv';
      $handle = fopen($download, "w");
      fwrite($handle, "Phenotypic Data for Germplasm '" . $stock->uniquename. "'\n");
      fwrite($handle, '"#","Dataset","Descriptor","Value","Environment","Replication","Plot"' . "\n");
      $counter = 0;
      foreach ($phenotypic_data as $score){
        $descriptor = $score->descriptor;
        $env = str_replace(array("COTTONDB_", "CDB_NCGC_"), array("", ""), $score->environment);
        fwrite($handle, '"' . ($counter + 1) . '","'. $score->project . '","' . $descriptor . '","' . $score->value . '","' . $env . '","' . $score->replications . '","' . $score->plot . '"' . "\n");
        $counter ++;
      }
      fclose($handle);
    ?>
    <div style="float: right">Download <a href="<?php print '/' . $download;?>">Table</a></div>
    <table id="tripal_stock-phenotypic_data-table" class="tripal_stock-table tripal-table tripal-table-horz" style="margin-bottom:20px;">
             <tr>
               <th>#</th>
               <th>Dataset</th>
               <th>Descriptor</th>
               <th>Value</th>
               <th>Environment</th>
               <th>Replication</th>
               <th>Plot</th>
             </tr>
    <?php
      $counter = 0;
      $class = "";
      foreach ($records as $score){
         if ($counter % 2 == 0) {
            $class = "tripal_stock-table-even-row even";
         } else {
            $class = "tripal_stock-table-odd-row odd";
         }
         $descriptor = $score->descriptor;
         $env = str_replace(array("COTTONDB_", "CDB_NCGC_"), array("", ""), $score->environment);
         $link = db_table_exists('chado_nd_geolocation') ? mainlab_tripal_link_record('nd_geolocation', $score->nd_geolocation_id) : NULL;
         $env_display = $link ? "<a href='$link'>" . $env . '</a>' : $env;
         $project = $score->project;
         $link = mainlab_tripal_link_record('project', $score->project_id);
         if ($link) {
           $project = "<a href=\"$link\">$project</a>";
         }
         print "<tr class=\"$class\"><td>". ($counter + 1) . "</td><td>$project</td><td>$descriptor</td><td>$score->value</td><td>$env_display</td><td>$score->replications</td><td>$score->plot</td></tr>";
         $counter ++;
      }
    ?>
    </table>
  </div>
 <?php
  print $pager['pager'];
}
