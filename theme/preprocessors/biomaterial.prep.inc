<?php

function mainlab_tripal_preprocess_mainlab_biomaterial_base (&$variables) {
    $node = $variables['node'];
    
    // Assay
    $sql = 
      'SELECT * FROM chado.assay A 
       INNER JOIN chado.assay_biomaterial AB ON A.assay_id = AB.assay_id
       WHERE biomaterial_id = :biomaterial_id';
    $results = db_query($sql, [':biomaterial_id' => $node->chado_record->biomaterial_id]);
    $assays = [];
    while ($ass = $results->fetchObject()) {
        $assays[$ass->ass_id] = $ass;
    }
    $node->chado_record->assays = $assays;
    if (db_table_exists('chado.expression_feature_all')) {
      $sql = 'SELECT analysis_id, analysis_name FROM chado.expression_feature_all WHERE biomaterial_id = :biomaterial_id LIMIT 1';
      $results = db_query($sql, [':biomaterial_id' => $node->chado_record->biomaterial_id]);
      $analysis = $results->fetchObject();
      $node->chado_record->analysis = $analysis;
    }
    else {
      $node->chado_record->analysis = NULL;
    }
}