<?php 

function mainlab_tripal_preprocess_mainlab_feature_bin_base (&$variables) {
  $feature = $variables['node']->feature;
  
  // Map info
  $result = db_query(
      "SELECT 
          FM.featuremap_id,
          FM.name AS map,
          (SELECT name FROM chado.feature WHERE feature_id = FP.map_feature_id) AS LG,
          (SELECT value FROM chado.featureposprop WHERE featurepos_id = FP.featurepos_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'start' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')) LIMIT 1) AS start,
          (SELECT value FROM chado.featureposprop WHERE featurepos_id = FP.featurepos_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')) LIMIT 1) AS stop
        FROM chado.featurepos FP
        INNER JOIN chado.featuremap FM ON FP.featuremap_id = FM.featuremap_id
        WHERE FP.feature_id = :feature_id
        LIMIT 1
      ", 
      array(':feature_id' => $feature->feature_id)
      )->fetchObject();
  $feature->featuremap_id = $result->featuremap_id;
  $feature->map = $result->map;
  $feature->lg = $result->lg;
  $feature->start = $result->start;
  $feature->stop = $result->stop;
  
  // Count Markers/QTL
  $num_loci = db_query(
      "SELECT count (F.uniquename)
        FROM {chado.featurepos} FP
        INNER JOIN {chado.feature} F ON F.feature_id = FP.feature_id
        INNER JOIN {chado.feature_relationship} FR ON FR.subject_id = FP.feature_id
        WHERE F.type_id = (SELECT cvterm_id
                                            FROM {chado.cvterm}
                                            WHERE name = 'marker_locus' AND cv_id = (SELECT cv_id FROM {chado.cv} WHERE name = 'MAIN'))
        AND FR.object_id = :bin_feature_id
        AND FR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'contained_in' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship'))
        AND FP.featuremap_id = :featuremap_id
        ", array(':bin_feature_id' => $feature->feature_id, ':featuremap_id' => $feature->featuremap_id))->fetchField();
  
  $num_qtl = db_query(
      "SELECT count (F.uniquename)
        FROM {chado.featurepos} FP
        INNER JOIN {chado.feature} F ON F.feature_id = FP.feature_id
        INNER JOIN {chado.feature_relationship} FR ON FR.subject_id = FP.feature_id
        WHERE F.type_id = (SELECT cvterm_id
                                            FROM {chado.cvterm}
                                            WHERE name = 'QTL' AND cv_id = (SELECT cv_id FROM {chado.cv} WHERE name = 'sequence'))
        AND FR.object_id = :bin_feature_id
        AND FR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'contained_in' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship'))
        AND FP.featuremap_id = :featuremap_id
        ", array(':bin_feature_id' => $feature->feature_id, ':featuremap_id' => $feature->featuremap_id))->fetchField();
  $feature->num_loci = $num_loci;
  $feature->num_qtl = $num_qtl;
}

function mainlab_tripal_get_bin_loci($feature_id, $featuremap_id, $limit, $page) {
  $sql = "
      SELECT
        F.uniquename AS LG,
        FR.object_id AS feature_id,
        (SELECT name FROM {feature} WHERE feature_id = FR.object_id) AS genetic_marker,
        F2.name AS marker,
        F2.feature_id AS marker_feature_id,
        (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) AS type,
        FPP.value AS position
      FROM {featurepos} FP
      INNER JOIN {feature} F ON F.feature_id = FP.map_feature_id
      INNER JOIN {feature} F2 ON F2.feature_id = FP.feature_id
      INNER JOIN {featureposprop} FPP ON FP.featurepos_id = FPP.featurepos_id AND FPP.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'start' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      INNER JOIN {feature_relationship} FR ON FR.subject_id = F2.feature_id AND FR.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'relationship'))
      WHERE FP.feature_id IN (SELECT subject_id FROM feature_relationship WHERE object_id = :feature_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'contained_in' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship')))
      AND FP.featuremap_id = :featuremap_id
      AND (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) IN ('marker_locus', 'heritable_phenotypic_marker') ORDER BY F.uniquename, FPP.value::float";
  $positions = mainlab_tripal_pager_query ($sql, array(':feature_id' => $feature_id, ':featuremap_id' => $featuremap_id), $limit, $page, TRUE);
  return $positions;
}

function mainlab_tripal_get_bin_QTL($feature_id, $featuremap_id, $limit, $page) {
  $sql = "
      SELECT
        F.uniquename AS LG,
        (SELECT first (object_id) FROM {feature_relationship} WHERE subject_id = F2.feature_id AND type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship'))) AS feature_id,
        F2.uniquename AS marker,
        F2.feature_id AS marker_feature_id,
        (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) AS type,
        FPP.value AS position,
        FPP2.value AS stop,
        FPP3.value AS peak
      FROM {featurepos} FP
      INNER JOIN {feature} F ON F.feature_id = FP.map_feature_id
      INNER JOIN {feature} F2 ON F2.feature_id = FP.feature_id
      LEFT JOIN {featureposprop} FPP ON FP.featurepos_id = FPP.featurepos_id AND FPP.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'start' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      LEFT JOIN {featureposprop} FPP2 ON FP.featurepos_id = FPP2.featurepos_id AND FPP2.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      LEFT JOIN {featureposprop} FPP3 ON FP.featurepos_id = FPP3.featurepos_id AND FPP3.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'qtl_peak' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      WHERE FP.feature_id IN (SELECT subject_id FROM feature_relationship WHERE object_id = :feature_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'contained_in' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship')))
      AND FP.featuremap_id = :featuremap_id
      AND (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) = 'QTL'";
  $positions = mainlab_tripal_pager_query ($sql, array(':feature_id' => $feature_id,':featuremap_id' => $featuremap_id), $limit, $page, TRUE);
  return $positions;
}