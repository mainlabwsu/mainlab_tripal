<?php 
/**
 * @ingroup tripal_stock
 */
function mainlab_tripal_preprocess_mainlab_project_QTL(&$variables) {
    $project = $variables['node']->project;
    $num_qtl = chado_query(
        "SELECT count (F.uniquename)
        FROM {feature_project} FP
        INNER JOIN {feature} F ON F.feature_id = FP.feature_id
        WHERE F.type_id = (SELECT cvterm_id
                                            FROM {cvterm}
                                            WHERE name = 'QTL' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'sequence'))
        AND project_id = :project_id", array(':project_id' => $project->project_id))->fetchField();
    $project->num_qtl = $num_qtl;
}


function mainlab_tripal_get_project_QTL($project_id, $limit, $page) {
    $sql = "
      SELECT
        F.uniquename AS LG,
        (SELECT first (object_id) FROM {feature_relationship} WHERE subject_id = F2.feature_id AND type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship'))) AS feature_id,
        F2.uniquename AS marker,
        F2.feature_id AS marker_feature_id,
        (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) AS type,
        FPP.value AS position,
        FPP2.value AS stop,
        FPP3.value AS peak
      FROM {featurepos} FP
      INNER JOIN {feature_project} FM ON FM.feature_id = FP.feature_id
      INNER JOIN {feature} F ON F.feature_id = FP.map_feature_id
      INNER JOIN {feature} F2 ON F2.feature_id = FP.feature_id
      LEFT JOIN {featureposprop} FPP ON FP.featurepos_id = FPP.featurepos_id AND FPP.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'start' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      LEFT JOIN {featureposprop} FPP2 ON FP.featurepos_id = FPP2.featurepos_id AND FPP2.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      LEFT JOIN {featureposprop} FPP3 ON FP.featurepos_id = FPP3.featurepos_id AND FPP3.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'qtl_peak' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      WHERE FM.project_id = :project_id
      AND (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) = 'QTL'";
    $positions = mainlab_tripal_pager_query ($sql, array(':project_id' => $project_id), $limit, $page, TRUE);
    return $positions;
}

function mainlab_tripal_preprocess_mainlab_project_stocks(&$variables) {
  $project = $variables['node']->project;
  $num_stocks = chado_query(
      "SELECT count(DISTINCT S.stock_id)
        FROM chado.stock S
        INNER JOIN chado.stock_relationship SR on SR.object_id = S.stock_id
        INNER JOIN chado.nd_experiment_stock NES on NES.stock_id = SR.subject_id
        INNER JOIN chado.nd_experiment_project NEP on NEP.nd_experiment_id = NES.nd_experiment_id
        WHERE NEP.project_id = :project_id", array(':project_id' => $project->project_id))->fetchField();
  $project->num_stocks = $num_stocks;
}

function mainlab_tripal_get_project_stocks($project_id, $limit, $page) {
  $sql = "
      SELECT S.stock_id, max(S.name) AS name, max(S.uniquename) AS uniquename, (SELECT name FROM chado.cvterm WHERE cvterm_id = S.type_id) AS type
        FROM chado.stock S
        INNER JOIN chado.stock_relationship SR on SR.object_id = S.stock_id
        INNER JOIN chado.nd_experiment_stock NES on NES.stock_id = SR.subject_id
        INNER JOIN chado.nd_experiment_project NEP on NEP.nd_experiment_id = NES.nd_experiment_id
        WHERE NEP.project_id = :project_id GROUP BY S.stock_id";
  $stocks = mainlab_tripal_pager_query ($sql, array(':project_id' => $project_id), $limit, $page, TRUE);
  return $stocks;
}

function mainlab_tripal_preprocess_mainlab_project_trait_descriptors(&$variables) {
  $project = $variables['node']->project;
  $num_descriptors = chado_query(
      "SELECT count(DISTINCT C1.cvterm_id)
    FROM {phenotype} P
    INNER JOIN {nd_experiment_phenotype} NEP ON P.phenotype_id = NEP.phenotype_id
    INNER JOIN {nd_experiment_project} NEJ ON NEP.nd_experiment_id = NEJ.nd_experiment_id
    INNER JOIN {cvterm} C1 ON C1.cvterm_id = P.attr_id 
    WHERE project_id = :project_id", array(':project_id' => $project->project_id))->fetchField();
  $project->num_descriptors = $num_descriptors;
}

function mainlab_tripal_get_project_trait_descriptors ($project_id, $limit, $page) {
  $sql = "
    SELECT DISTINCT 
      C1.cvterm_id,
      CASE
        WHEN (SELECT value FROM {cvprop} WHERE cv_id = C1.cv_id AND type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'searchable' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))) IS NOT NULL
        THEN   (SELECT value FROM {cvprop} WHERE cv_id = C1.cv_id AND type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'searchable' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')))
        ELSE     (SELECT name FROM {cv} WHERE cv_id = C1.cv_id)
        END
      AS group,
      C1.name AS descriptor
    FROM {phenotype} P
    INNER JOIN {nd_experiment_phenotype} NEP ON P.phenotype_id = NEP.phenotype_id
    INNER JOIN {nd_experiment_project} NEJ ON NEP.nd_experiment_id = NEJ.nd_experiment_id
    INNER JOIN {cvterm} C1 ON C1.cvterm_id = P.attr_id 
    WHERE project_id = :project_id
  ";
  $descriptors = mainlab_tripal_pager_query ($sql, array(':project_id' => $project_id), $limit, $page, TRUE);
  return $descriptors;
}

function mainlab_tripal_preprocess_mainlab_project_traits(&$variables) {
  $project = $variables['node']->project;
  $num_traits = chado_query(
      "SELECT count(DISTINCT V.cvterm_id)
          FROM chado.cvterm V
          INNER JOIN chado.feature_cvterm FV ON FV.cvterm_id = V.cvterm_id
          INNER JOIN chado.feature_project FJ ON FJ.feature_id = FV.feature_id
          WHERE FJ.project_id = :project_id", array(':project_id' => $project->project_id))->fetchField();
  $project->num_traits = $num_traits;
}

function mainlab_tripal_get_project_traits ($project_id, $limit, $page) {
  $sql = "
   SELECT DISTINCT
            V.cvterm_id, V.name AS trait
          FROM chado.cvterm V
          INNER JOIN chado.feature_cvterm FV ON FV.cvterm_id = V.cvterm_id
          INNER JOIN chado.feature_project FJ ON FJ.feature_id = FV.feature_id
          WHERE FJ.project_id = :project_id
  ";
  $traits = mainlab_tripal_pager_query ($sql, array(':project_id' => $project_id), $limit, $page, TRUE);
  return $traits;
}

function mainlab_tripal_preprocess_mainlab_project_genetic_maps(&$variables) {
  $project = $variables['node']->project;
  $num_maps = chado_query(
      "SELECT count(DISTINCT FM.featuremap_id)
          FROM chado.project_pub PP 
          INNER JOIN chado.featuremap_pub FMP ON PP.pub_id = FMP.pub_Id
          INNER JOIN chado.featuremap FM ON FM.featuremap_id = FMP.featuremap_id
          WHERE project_id = :project_id", array(':project_id' => $project->project_id))->fetchField();
  $project->num_maps = $num_maps;
}

function mainlab_tripal_get_project_genetic_maps ($project_id, $limit, $page) {
  $sql = "
   SELECT DISTINCT
            FM.featuremap_id, FM.name 
          FROM chado.project_pub PP 
          INNER JOIN chado.featuremap_pub FMP ON PP.pub_id = FMP.pub_Id
          INNER JOIN chado.featuremap FM ON FM.featuremap_id = FMP.featuremap_id
          WHERE project_id = :project_id
  ";
  $featuremaps = mainlab_tripal_pager_query ($sql, array(':project_id' => $project_id), $limit, $page, TRUE);
  return $featuremaps;
}