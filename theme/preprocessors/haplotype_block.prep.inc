<?php

/**
 * @ingroup tripal_feature
 */
function mainlab_tripal_preprocess_mainlab_feature_haplotype_block_base(&$variables) {
  $feature = $variables['node']->feature;

  // Get stocks
  $haplotypes = mainlab_tripal_get_haplotypes($feature->feature_id);
  $feature->stocks = $haplotypes;

  // Get Haplotypes

  // Get projects
  $sql = "SELECT DISTINCT P.project_id, P.name FROM {feature_genotype} FG
      INNER JOIN {nd_experiment_genotype} NEG ON NEG.genotype_id = FG.genotype_id
      INNER JOIN {nd_experiment_project} NEP ON NEG.nd_experiment_id = NEP.nd_experiment_id
      INNER JOIN {project} P ON P.project_id = NEP.project_id
      WHERE feature_id =:feature_id";
  $result = chado_query($sql, array(':feature_id' => $feature->feature_id));
  $projects = array();
  while ($project = $result->fetchObject()) {
      array_push($projects, $project);
  }
  $feature->projects = $projects;

  // Get haplotypes
  $sql =
  "SELECT
    (SELECT replace (uniquename, HB.uniquename || '_', '') FROM {feature} WHERE feature_id = FR.subject_id) AS haplotype,
    FR2P.value,
    (SELECT feature_id FROM {feature} WHERE feature_id = FR2.object_id) AS marker_feature_id,
    (SELECT nid FROM chado_feature WHERE feature_id = FR2.object_id) AS marker_nid,
    (SELECT uniquename FROM {feature} WHERE feature_id = FR2.object_id) AS marker
  FROM {feature} HB
  LEFT JOIN {feature_relationship} FR ON HB.feature_id = FR.object_id AND FR.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'variant_of' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'sequence'))
  LEFT JOIN {feature_relationship} FR2 ON FR2.subject_id = FR.subject_id AND FR2.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'contains' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'sequence'))
  LEFT JOIN {feature_relationshipprop} FR2P ON FR2.feature_relationship_id = FR2P.feature_relationship_id
  WHERE feature_id = :feature_id ORDER BY marker";
  $result = chado_query($sql, array(':feature_id' => $feature->feature_id));
  $markers = array();
  while ($record = $result->fetchObject()) {
    $marker_feature_id = $record->marker_feature_id;
    $marker = $record->marker;
    $marker_nid = $record->marker_nid;
    $h = $record->haplotype;
    $v = $record->value;
    // Exisiting marker, add haplotype
    if (array_key_exists($marker_feature_id, $markers)) {
      $oldm = $markers[$marker_feature_id];
      // If haplotype exists, add new value
      if (array_key_exists($h, $oldm->haplotypes)) {
        $oldv = $oldm->haplotypes[$h];
        $oldm->haplotypes[$h] = $oldv . '; ' . $v;
      } // create new haplotype
      else {
        $oldm->haplotypes[$h] = $v;
      }
      $markers[$marker_feature_id] = $oldm;
      // New marker, create haplotype
    }
    else {
      $m = new stdClass();
      $m->name = $marker;
      $m->nid = $marker_nid;
      $m->haplotypes = array ();
      $m->haplotypes [$h]= $v;
      $markers[$marker_feature_id] = $m;
    }
  }
  $feature->haplotypes = $markers;
}

/**
 *
 *
 * @ingroup tripal_feature
 */
function mainlab_tripal_preprocess_mainlab_feature_haplotype_block_map_positions(&$variables) {
  $feature = $variables['node']->feature;
  // get map positions
  $results = chado_query(
      "SELECT
      FM.featuremap_id,
      FM.name AS map,
      LG.name AS linkage_group,
      FPP.value AS start,
      FPP2.value AS stop,
      LOCUS.name AS name
    FROM {feature} LOCUS
    INNER JOIN {featurepos} FP ON LOCUS.feature_id = FP.feature_id
    INNER JOIN {featuremap} FM ON FP.featuremap_id = FM.featuremap_id
    INNER JOIN {feature} LG ON FP.map_feature_id = LG.feature_id
    INNER JOIN {featureposprop} FPP ON FP.featurepos_id = FPP.featurepos_id
    INNER JOIN {featureposprop} FPP2 ON FP.featurepos_id = FPP2.featurepos_id
    WHERE
     FPP.type_id =
     (SELECT cvterm_id
      FROM {cvterm}
      WHERE name = 'start'
     AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
     )
    AND
      FPP2.type_id =
     (SELECT cvterm_id
      FROM {cvterm}
      WHERE name = 'stop'
     AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
     )
    AND LOCUS.feature_id = :feature_id", array(':feature_id' => $feature->feature_id));
  $positions = array ();
  $counter = 0;
  while ($pos = $results->fetchObject()) {
    $positions [$counter] = $pos;
    $counter ++;
  }
  $feature->map_positions = $positions;
}

function mainlab_tripal_get_haplotypes($feature_id) {
  $sql = "
    SELECT
      S.stock_id,
      S.uniquename AS stock,
      string_agg (DISTINCT H.name, ' ') AS haplotype
      FROM chado.feature HB
      INNER JOIN chado.feature_relationship FR ON HB.feature_id = FR.object_id AND FR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'variant_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence'))
      INNER JOIN chado.feature H ON H.feature_id = FR.subject_id
      INNER JOIN chado.feature_genotype FG ON FG.feature_id = HB.feature_id
      INNER JOIN chado.nd_experiment_genotype NEG ON NEG.genotype_id = FG.genotype_id
      INNER JOIN chado.genotype G ON G.genotype_id = NEG.genotype_id
      INNER JOIN chado.nd_experiment_stock NES ON NEG.nd_experiment_id = NES.nd_experiment_id
      INNER JOIN chado.stock S ON S.stock_id = NES.stock_id
    WHERE lower(H.name) = ANY(string_to_array(lower(G.description), '|'))
    AND HB.feature_id = :feature_id
    GROUP BY S.stock_id, S.uniquename
  ";
  $results = db_query($sql, array(':feature_id' => $feature_id));
  $haplotypes = array();
  while ($obj = $results->fetchObject()) {
    $haplotypes [] = $obj;
  }
  return $haplotypes;
}