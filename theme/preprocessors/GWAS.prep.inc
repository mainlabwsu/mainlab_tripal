<?php

/**
 * Provide more details about the GWAS
 *
 * @param $variables
 */
function mainlab_tripal_preprocess_mainlab_feature_GWAS_base(&$variables) {
  $feature = $variables['node']->feature;
  $feature_id = $feature->feature_id;
  $gwas = new stdClass();
  // Get GWAS Study
  $sql =
  "SELECT P.project_id, P.name FROM {chado.project} P
          INNER JOIN {chado.feature_project} FP ON FP.project_id = P.project_id
          WHERE feature_id = :feature_id";
  $study = db_query($sql, array(':feature_id' => $feature_id))->fetchObject();
  $gwas->study = $study->name;
  $gwas->study_id = $study->project_id;

  // Get gwas markers
  $sql = "
     SELECT
       MARKER.feature_id,
       MARKER.uniquename AS gwas_marker
     FROM {feature} GWAS
     INNER JOIN {feature_relationship} FR ON GWAS.feature_id = FR.object_id
     INNER JOIN {feature} MARKER ON MARKER.feature_id = FR.subject_id
     INNER JOIN {cvterm} V ON V.cvterm_id = FR.type_id
     WHERE V.name = 'associated_with' AND V.cv_id = (SELECT cv_id FROM {cv} WHERE name = 'sequence')
     AND MARKER.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'genetic_marker' AND V.cv_id = (SELECT cv_id FROM {cv} WHERE name = 'sequence'))
     AND GWAS.feature_id = :feature_id
   ";
  $gwas->gwas_marker = mainlab_tripal_dbresult_to_array(chado_query($sql, array(':feature_id' => $feature_id)));

  // Get aliases
  $sql = "
    SELECT name
    FROM {synonym} S
      INNER JOIN {feature_synonym} FS ON S.synonym_id = FS.synonym_id
    WHERE FS.feature_id = :feature_id
  ";
  $gwas->synonyms = mainlab_tripal_dbresult_to_array(chado_query($sql, array(':feature_id' => $feature_id)));

  // Get Panel
  $sql = "
    SELECT uniquename, S.stock_id
    FROM {stock} S
      INNER JOIN {feature_stock} FS ON S.stock_id = FS.stock_id
    WHERE FS.feature_id = :feature_id
    AND S.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'panel' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
  ";
  $gwas->panel = chado_query($sql, array(':feature_id' => $feature_id))->fetchObject();

  // Get Trait
  $sql = "
    SELECT name, V.cvterm_id
    FROM {cvterm} V
      INNER JOIN {feature_cvterm} FV ON V.cvterm_id = FV.cvterm_id
    WHERE FV.feature_id = :feature_id
  ";
  $gwas->trait = chado_query($sql, array(':feature_id' => $feature_id))->fetchObject();

  // Get Genome
  $sql = "
    SELECT A.name, A.analysis_id
    FROM {analysis} A
    WHERE A.analysis_id = (
      SELECT NULLIF(value, '')::int
      FROM {projectprop}
      WHERE
        type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'analysis_id' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      AND project_id = :project_id
    )
  ";
  $gwas->genome = chado_query($sql, array(':project_id' => $study->project_id))->fetchObject();

  $sql = "
  SELECT
    LM.name AS landmark,
    srcfeature_id,
    (SELECT name FROM cvterm WHERE cvterm_id = LM.type_id) AS type,
    fmin,
    fmax,
    A.analysis_id,
    A.name AS analysis,
    AP.value AS jbrowse_url
  FROM {featureloc} LOC
  INNER JOIN feature LM ON LM.feature_id = LOC.srcfeature_id
  INNER JOIN analysisfeature AF ON AF.feature_id = LM.feature_id
  INNER JOIN analysis A ON A.analysis_id = AF.analysis_id
  LEFT JOIN analysisprop AP ON AP.analysis_id = A.analysis_id AND AP.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'JBrowse URL')
  WHERE LOC.feature_id = :feature_id
  ";
  $gmarker = isset($gwas->gwas_marker[0]) ? $gwas->gwas_marker[0] : NULL;
  $gwas_loc = $gmarker? mainlab_tripal_dbresult_to_array(chado_query($sql, array(':feature_id' => $gmarker->feature_id))) : '';

  $feature->mainlab_gwas = new stdClass();
  $feature->mainlab_gwas->gwas_details =  $gwas;
  $feature->mainlab_gwas->locations = $gwas_loc;
}

/**
 * Provide more details about the GWAS
 *
 * @param $variables
 */
function mainlab_tripal_preprocess_mainlab_feature_GWAS_genome_location(&$variables) {

}
