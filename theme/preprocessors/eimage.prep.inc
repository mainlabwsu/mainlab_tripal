<?php 
function mainlab_tripal_preprocess_mainlab_eimage_base(&$variables) {
    $eimage = $variables['node']->eimage;
    // Update Tripal entity title if it's not the file name
    $sql = "SELECT title FROM tripal_entity WHERE id = :entity_id";
    $title = db_query($sql, array(':entity_id' => $eimage->entity_id))->fetchField();
    if ($title != $eimage->eimage_data) {
        db_query("UPDATE tripal_entity SET title = '". $eimage->eimage_data . "' WHERE id = " . $eimage->entity_id);
        $link = mainlab_tripal_link_record('eimage', $eimage->eimage_id);
        drupal_goto($link);
    }
    
    // Get eimageprop
    $props = array();
    if (db_table_exists('chado.eimageprop')) {
        $sql = "SELECT (SELECT name FROM chado.cvterm WHERE cvterm_id = type_id), value FROM chado.eimageprop WHERE eimage_id = :eimage_id";
        $results = db_query($sql, array(':eimage_id' => $eimage->eimage_id));
        while ($prop = $results->fetchObject()) {
            $props [$prop->name] = $prop->value;
        }
    }
    $eimage->properties = $props;
    
    // Get contacts
    $contacts = array();
    if (db_table_exists('chado.contact_image')) {
        $sql = "SELECT (SELECT name FROM chado.contact WHERE contact_id = CI.contact_id), CI.contact_id FROM chado.contact_image CI WHERE eimage_id = :eimage_id";
        $results = db_query($sql, array(':eimage_id' => $eimage->eimage_id));
        while ($contact = $results->fetchObject()) {
            $contacts [$contact->contact_id] = $contact->name;
        }
    }
    $eimage->contacts = $contacts;
    
    // Get species
    $orgs = array();
    if (db_table_exists('chado.organism_image')) {
        $sql = "SELECT (SELECT species FROM chado.organism WHERE organism_id = OI.organism_id), OI.organism_id FROM chado.organism_image OI WHERE eimage_id = :eimage_id";
        $results = db_query($sql, array(':eimage_id' => $eimage->eimage_id));
        while ($organism = $results->fetchObject()) {
            $orgs [$organism->organism_id] = $organism->species;
        }
    }
    $eimage->orgs = $orgs;
    
    // Get pubs
    $pubs = array();
    if (db_table_exists('chado.pub_image')) {
        $sql = "SELECT (SELECT uniquename FROM chado.pub WHERE pub_id = PI.pub_id), PI.pub_id FROM chado.pub_image PI WHERE eimage_id = :eimage_id";
        $results = db_query($sql, array(':eimage_id' => $eimage->eimage_id));
        while ($pub = $results->fetchObject()) {
            $pubs [$pub->pub_id] = $pub->uniquename;
        }
    }
    $eimage->pubs = $pubs;
    
    // Get projects
    $projects = array();
    if (db_table_exists('chado.project_image')) {
        $sql = "SELECT (SELECT name FROM chado.project WHERE project_id = PI.project_id), PI.project_id FROM chado.project_image PI WHERE eimage_id = :eimage_id";
        $results = db_query($sql, array(':eimage_id' => $eimage->eimage_id));
        while ($project = $results->fetchObject()) {
            $projects [$project->project_id] = $project->name;
        }
    }
    $eimage->projects = $projects;
    
    // Get germplasm
    $stocks = array();
    if (db_table_exists('chado.stock_image')) {
        $sql = "SELECT (SELECT uniquename FROM chado.stock WHERE stock_id = SI.stock_id), SI.stock_id FROM chado.stock_image SI WHERE eimage_id = :eimage_id";
        $results = db_query($sql, array(':eimage_id' => $eimage->eimage_id));
        while ($stock = $results->fetchObject()) {
            $stocks [$stock->stock_id] = $stock->uniquename;
        }
    }
    $eimage->stocks = $stocks;
    
    // Get markers
    $markers = array();
    if (db_table_exists('chado.feature_image')) {
        $sql = "SELECT F.name, FI.feature_id FROM chado.feature_image FI INNER JOIN chado.feature F ON F.feature_id = FI.feature_id WHERE eimage_id = :eimage_id 
                  AND type_id IN (SELECT cvterm_id FROM chado.cvterm WHERE name = 'genetic_marker')";
        $results = db_query($sql, array(':eimage_id' => $eimage->eimage_id));
        while ($feature = $results->fetchObject()) {
            $markers [$feature->feature_id] = $feature->name;
        }
    }
    $eimage->markers = $markers;
    
    // Get MTL
    $mtls = array();
    if (db_table_exists('chado.feature_image')) {
        $sql = "SELECT F.name, FI.feature_id FROM chado.feature_image FI INNER JOIN chado.feature F ON F.feature_id = FI.feature_id WHERE eimage_id = :eimage_id
                  AND type_id IN (SELECT cvterm_id FROM chado.cvterm WHERE name = 'heritable_phenotypic_marker')";
        $results = db_query($sql, array(':eimage_id' => $eimage->eimage_id));
        while ($feature = $results->fetchObject()) {
            $mtls [$feature->feature_id] = $feature->name;
        }
    }
    $eimage->mtls = $mtls;
    
    // Get traits & descriptors
    $traits = array();
    $descriptors = array();
    if (db_table_exists('chado.cvterm_image')) {
        $sql = "SELECT V.name, CI.cvterm_id, TYPE.name AS type FROM chado.cvterm_image CI 
                  INNER JOIN chado.cvterm V ON V.cvterm_id = CI.cvterm_id
                  INNER JOIN chado.cvterm_relationship CR ON CR.subject_id = V.cvterm_id
                  INNER JOIN chado.cvterm TYPE on TYPE.cvterm_id = CR.type_id 
                   WHERE eimage_id = :eimage_id";
        $results = db_query($sql, array(':eimage_id' => $eimage->eimage_id));
        while ($cvterm = $results->fetchObject()) {
          if ($cvterm->type == 'is_a') {
            $traits [$cvterm->cvterm_id] = $cvterm->name;
          }
          else if ($cvterm->type == 'belongs_to') {
            $descriptors [$cvterm->cvterm_id] = $cvterm->name;
          }
        }
    }
    $eimage->traits = $traits;
    $eimage->descriptors = $descriptors;
}