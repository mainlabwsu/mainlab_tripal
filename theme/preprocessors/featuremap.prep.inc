<?php 

/**
 * @ingroup mainlab_featuremap
 */
function mainlab_tripal_preprocess_mainlab_featuremap_base(&$variables) {
  $featuremap = $variables['node']->featuremap;
  $num_loci = chado_query(
      "SELECT count (F.uniquename)
        FROM {featurepos} FP
        INNER JOIN {feature} F ON F.feature_id = FP.feature_id
        WHERE F.type_id = (SELECT cvterm_id
                                            FROM {cvterm}
                                            WHERE name = 'marker_locus' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
        AND featuremap_id = :featuremap_id", array(':featuremap_id' => $featuremap->featuremap_id))->fetchField();
  $num_qtl = chado_query(
      "SELECT count (F.uniquename)
        FROM {featurepos} FP
        INNER JOIN {feature} F ON F.feature_id = FP.feature_id
        WHERE F.type_id = (SELECT cvterm_id
                                            FROM {cvterm}
                                            WHERE name = 'QTL' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'sequence'))
        AND featuremap_id = :featuremap_id", array(':featuremap_id' => $featuremap->featuremap_id))->fetchField();
  $num_lg = chado_query(
      "SELECT count (distinct F.uniquename)
        FROM {featurepos} FP
        INNER JOIN {feature} F ON F.feature_id = FP.map_feature_id
        WHERE F.type_id = (SELECT cvterm_id
                                            FROM {cvterm}
                                            WHERE name = 'linkage_group' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'sequence'))
        AND featuremap_id = :featuremap_id", array('featuremap_id' => $featuremap->featuremap_id))->fetchField();
  $cmap_r = chado_query(
      "SELECT db.urlprefix, X.accession
       FROM {featuremap_dbxref} FD
       INNER JOIN {dbxref} X ON FD.dbxref_id = X.dbxref_id
       INNER JOIN {db} ON db.db_id = X.db_id
       WHERE featuremap_id = :featuremap_id
      ", array(':featuremap_id' => $featuremap->featuremap_id));
  $cmap = $cmap_r->fetchObject();
  $cmap_url = is_object($cmap) ? $cmap->urlprefix . $cmap->accession : NULL;

  $featuremap->num_loci = $num_loci;
  $featuremap->num_qtl = $num_qtl;
  $featuremap->num_lg = $num_lg;
  $featuremap->cmap_url = $cmap_url;
}

function mainlab_tripal_get_featuremap_loci($featuremap_id, $limit, $page) {
  $sql = "
      SELECT
        F.uniquename AS LG,
        FR.object_id AS feature_id,
        (SELECT name FROM {feature} WHERE feature_id = FR.object_id) AS genetic_marker,
        F2.name AS marker,
        F2.feature_id AS marker_feature_id,
        (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) AS type,
        FPP.value AS position
      FROM {featurepos} FP
      INNER JOIN {featuremap} FM ON FM.featuremap_id = FP.featuremap_id
      INNER JOIN {feature} F ON F.feature_id = FP.map_feature_id
      INNER JOIN {feature} F2 ON F2.feature_id = FP.feature_id
      INNER JOIN {featureposprop} FPP ON FP.featurepos_id = FPP.featurepos_id AND FPP.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'start' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      INNER JOIN {feature_relationship} FR ON FR.subject_id = F2.feature_id AND FR.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'relationship')) 
      WHERE FM.featuremap_id = :featuremap_id
      AND (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) IN ('marker_locus', 'heritable_phenotypic_marker') ORDER BY F.uniquename, FPP.value::float";
  $positions = mainlab_tripal_pager_query ($sql, array(':featuremap_id' => $featuremap_id), $limit, $page, TRUE);
  return $positions;
}

function mainlab_tripal_get_featuremap_QTL($featuremap_id, $limit, $page) {
    $sql = "
      SELECT
        F.uniquename AS LG,
        (SELECT first (object_id) FROM {feature_relationship} WHERE subject_id = F2.feature_id AND type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship'))) AS feature_id,
        F2.uniquename AS marker,
        F2.feature_id AS marker_feature_id,
        (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) AS type,
        FPP.value AS position,
        FPP2.value AS stop,
        FPP3.value AS peak
      FROM {featurepos} FP
      INNER JOIN {featuremap} FM ON FM.featuremap_id = FP.featuremap_id
      INNER JOIN {feature} F ON F.feature_id = FP.map_feature_id
      INNER JOIN {feature} F2 ON F2.feature_id = FP.feature_id
      LEFT JOIN {featureposprop} FPP ON FP.featurepos_id = FPP.featurepos_id AND FPP.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'start' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      LEFT JOIN {featureposprop} FPP2 ON FP.featurepos_id = FPP2.featurepos_id AND FPP2.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      LEFT JOIN {featureposprop} FPP3 ON FP.featurepos_id = FPP3.featurepos_id AND FPP3.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'qtl_peak' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      WHERE FM.featuremap_id = :featuremap_id
      AND (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) = 'QTL'";
    $positions = mainlab_tripal_pager_query ($sql, array(':featuremap_id' => $featuremap_id), $limit, $page, TRUE);
    return $positions;
}