<?php
$eimage  = $variables['node']->eimage;
$projects = $eimage->projects;
if (mainlab_tripal_count($projects) > 0) {
    $rows = array();
    foreach ($projects AS $id => $proj) {
        $link = mainlab_tripal_link_record('project', $id);
        if ($link) {
          $rows [] = array ("<a href='$link'>". $proj . '</a>');  
        }
        else {
          $rows [] = array ($proj);
        }
    }
    $header = array ('Name');
    $table = array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array(
            'id' => 'tripal_feature-table-project',
        ),
        'sticky' => FALSE,
        'caption' => '',
        'colgroups' => array(),
        'empty' => '',
    );
    print theme_table($table);
}
?>