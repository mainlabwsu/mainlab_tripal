<?php
$eimage  = $variables['node']->eimage;
$descriptors = $eimage->descriptors;
if (mainlab_tripal_count($descriptors) > 0) {
    $rows = array();
    foreach ($descriptors AS $id => $name) {
        $link = '/trait_descriptor/' . $id;
        if ($link) {
          $rows [] = array ("<a href='$link'>". $name . '</a>');  
        }
        else {
          $rows [] = array ($name);
        }
    }
    $header = array ('Name');
    $table = array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array(
            'id' => 'tripal_feature-table-descriptor',
        ),
        'sticky' => FALSE,
        'caption' => '',
        'colgroups' => array(),
        'empty' => '',
    );
    print theme_table($table);
}
?>