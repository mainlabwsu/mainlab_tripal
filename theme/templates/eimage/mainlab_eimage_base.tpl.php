<?php

//$l = mainlab_tripal_link_record('eimage', 12472);dpm($l);

$node = $variables['node'];
$eimage = $variables['node']->eimage;

$props = $eimage->properties;
$image_type = isset($props['image_type']) ? $props['image_type'] : NULL;
$legend = isset($props['legend']) ? $props['legend'] : 'N/A';


// image
$icon_path = '';
$img_path = '';
if (mainlab_tripal_get_site() == 'cottongen') {
    if ($image_type == 'stock') {
        $icon_path ="sites/default/files/bulk_data/www.cottongen.org/cotton_photo/germplasm/icon/icon-" . $eimage->eimage_data;
        $img_path = "sites/default/files/bulk_data/www.cottongen.org/cotton_photo/germplasm/image/" . $eimage->eimage_data;
    }
    else {
      $icon_path ="sites/default/files/bulk_data/www.cottongen.org/cotton_photo/z_" . $image_type . "/icon/icon-" . $eimage->eimage_data;
      $img_path = "sites/default/files/bulk_data/www.cottongen.org/cotton_photo/z_" . $image_type . "/image/" . $eimage->eimage_data;
    }
    $alt_ico_path = "sites/default/files/bulk_data/www.cottongen.org/cotton_photo/non-ncgc/icon/icon-" . $eimage->eimage_data;
    $alt_img_path = "sites/default/files/bulk_data/www.cottongen.org/cotton_photo/non-ncgc/image/" . $eimage->eimage_data;
}

//Contacts
$contacts = $eimage->contacts;
$contact = '';
foreach ($contacts AS $id => $con) {
    $clink = mainlab_tripal_link_record('contact', $id);
    if ($clink) {
      $contact .= "<a href='$clink'>" . $con . "<br>";
    }
    else {
      $contact .= $con . "<br>";
    }
}

//Species
$orgs = $eimage->orgs;
$organism = '';
foreach ($orgs AS $id => $org) {
    $olink = mainlab_tripal_link_record('organism', $id);
    if ($olink) {
        $organism .= "<a href='$olink'>" . $org . "<br>";
    }
    else {
        $organism .= $org . "<br>";
    }
}

//Pubs
$pubs = $eimage->pubs;
$pub = '';
foreach ($pubs AS $id => $con) {
    $clink = mainlab_tripal_link_record('pub', $id);
    if ($clink) {
        $pub .= "<a href='$clink'>" . $con . "<br>";
    }
    else {
        $pub .= $con . "<br>";
    }
}

//Projects
$projects = $eimage->projects;

// Stocks
$stocks = $eimage->stocks;

// Markers
$markers = $eimage->markers;

// MTLs
$mtls = $eimage->mtls;

// Traits
$traits = $eimage->traits;

// Descriptors
$descriptors = $eimage->descriptors;

?>
<div id="tripal_eimage-base-box" class="tripal_eimage-info-box tripal-info-box">

  <table id="tripal_eimage-table-base" class="tripal_eimage-table tripal-table tripal-table-vert">
  <?php 
  if (file_exists($icon_path)) {
    $iconurl = url($icon_path);
    $imgurl = url($img_path); ?>
    <tr class="tripal_eimage-table-even-row even">
      <th width=30%><a href=<?php print$imgurl; ?> target=_blank><img src="<?php print $iconurl; ?>" style="cursor:pointer;"></a></th>
      <td></td>
    </tr>  
  <?php }
  else if (file_exists($alt_ico_path)) {
    $iconurl = url($alt_ico_path);
    $imgurl = url($alt_img_path); ?>
    <tr class="tripal_eimage-table-even-row even">
      <th width=30%><a href=<?php print$imgurl; ?> target=_blank><img src="<?php print $iconurl; ?>" style="cursor:pointer;"></a></th>
      <td></td>
    </tr>    
  <?php } else { ?>
    <tr class="tripal_eimage-table-even-row even">
      <th width=30%>Image Name</th>
      <td><?php print $eimage->image_uri; ?></td>
    </tr>
  <?php } ?>
    <tr class="tripal_eimage-table-odd-row odd">
      <th>Legend</th>
      <td><?php print $legend?></td>
    </tr>
    
    <?php 
      $class_even = "tripal_eimage-table-even-row even";
      $class_odd = "tripal_eimage-table-odd-row odd";
      $class_counter = 0;
      
      // Contact
      if (mainlab_tripal_count($contacts) > 0) {
        $class = $class_counter % 2 == 0 ? $class_even : $class_odd;
        $class_counter ++;
        print "<tr class=\"$class\"><th>Contact</th><td>$contact</td></tr>";
      }
      // Species
      if (mainlab_tripal_count($orgs) > 0) {
          $class = $class_counter % 2 == 0 ? $class_even : $class_odd;
          $class_counter ++;
          print "<tr class=\"$class\"><th>Species</th><td>$organism</td></tr>";
      }
      // Pubs
      if (mainlab_tripal_count($pubs) > 0) {
          $class = $class_counter % 2 == 0 ? $class_even : $class_odd;
          $class_counter ++;
          print "<tr class=\"$class\"><th>Publication</th><td>$pub</td></tr>";
      }
      // Projects
      $num_projects = mainlab_tripal_count($projects);
      if ($num_projects > 0) {
          $class = $class_counter % 2 == 0 ? $class_even : $class_odd;
          $class_counter ++;
          print "<tr class=\"$class\"><th>In Dataset</th><td>[<a href='?pane=in_dataset'>view all $num_projects</a>]</td></tr>";
      }
      // Stocks
      $num_stocks = mainlab_tripal_count($stocks);
      if ($num_stocks > 0) {
          $class = $class_counter % 2 == 0 ? $class_even : $class_odd;
          $class_counter ++;
          print "<tr class=\"$class\"><th>Germplasm</th><td>[<a href='?pane=stock'>view all $num_stocks</a>]</td></tr>";
      }      
      // Markers
      $num_markers = mainlab_tripal_count($markers);
      if ($num_markers > 0) {
          $class = $class_counter % 2 == 0 ? $class_even : $class_odd;
          $class_counter ++;
          print "<tr class=\"$class\"><th>Marker</th><td>[<a href='?pane=marker'>view all $num_markers</a>]</td></tr>";
      }
      // MTLs
      $num_mtls = mainlab_tripal_count($mtls);
      if ($num_mtls > 0) {
          $class = $class_counter % 2 == 0 ? $class_even : $class_odd;
          $class_counter ++;
          print "<tr class=\"$class\"><th>MTL</th><td>[<a href='?pane=mtl'>view all $num_mtls</a>]</td></tr>";
      }
      // Traits
      $num_traits = mainlab_tripal_count($traits);
      if ($num_traits > 0) {
          $class = $class_counter % 2 == 0 ? $class_even : $class_odd;
          $class_counter ++;
          print "<tr class=\"$class\"><th>Trait</th><td>[<a href='?pane=trait'>view all $num_traits</a>]</td></tr>";
      }
      // Descriptors
      $num_descriptors = mainlab_tripal_count($descriptors);
      if ($num_descriptors > 0) {
          $class = $class_counter % 2 == 0 ? $class_even : $class_odd;
          $class_counter ++;
          print "<tr class=\"$class\"><th>Triat Descriptor</th><td>[<a href='?pane=trait_descriptor'>view all $num_descriptors</a>]</td></tr>";
      }
    ?>
      
  </table> 
  <?php
  $image = $eimage->image_uri;
  if ($pub_img) {
    print "<div style=\"margin-top:25px;clear:both;\"><a href=\"/$pub_img\" target=\"_blank\"><img src=\"/$pub_img\" width=\"100%\" style=\"max-width:800px\"></a></div>";
  }
  if ($marker_img) {
    print "<div style=\"margin-top:25px;clear:both;\"><a href=\"/$marker_img\" target=\"_blank\"><img src=\"/$marker_img\" width=\"100%\" style=\"max-width:800px\"></a></div>";
  }
  if ($stock_img) {
    print "<div style=\"margin-top:25px;clear:both;\"><a href=\"/$stock_img\" target=\"_blank\"><img src=\"/$stock_img\" width=\"100%\" style=\"max-width:800px\"></a></div>";
  }
  ?>
</div>
