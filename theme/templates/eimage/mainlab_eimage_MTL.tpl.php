<?php
$eimage  = $variables['node']->eimage;
$mtls = $eimage->mtls;
if (mainlab_tripal_count($mtls) > 0) {
    $rows = array();
    foreach ($mtls AS $id => $name) {
        $link = mainlab_tripal_link_record('feature', $id);
        if ($link) {
          $rows [] = array ("<a href='$link'>". $name . '</a>');  
        }
        else {
          $rows [] = array ($name);
        }
    }
    $header = array ('Name');
    $table = array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array(
            'id' => 'tripal_feature-table-mtl',
        ),
        'sticky' => FALSE,
        'caption' => '',
        'colgroups' => array(),
        'empty' => '',
    );
    print theme_table($table);
}
?>