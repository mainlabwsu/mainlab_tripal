<?php
$eimage  = $variables['node']->eimage;
$stocks = $eimage->stocks;
if (mainlab_tripal_count($stocks) > 0) {
    $rows = array();
    foreach ($stocks AS $id => $name) {
        $link = mainlab_tripal_link_record('stock', $id);
        if ($link) {
          $rows [] = array ("<a href='$link'>". $name . '</a>');  
        }
        else {
          $rows [] = array ($name);
        }
    }
    $header = array ('Name');
    $table = array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array(
            'id' => 'tripal_feature-table-stock',
        ),
        'sticky' => FALSE,
        'caption' => '',
        'colgroups' => array(),
        'empty' => '',
    );
    print theme_table($table);
}
?>