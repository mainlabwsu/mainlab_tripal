<?php
$eimage  = $variables['node']->eimage;
$traits = $eimage->traits;
if (mainlab_tripal_count($traits) > 0) {
    $rows = array();
    foreach ($traits AS $id => $name) {
        $link = '/trait/' . $id;
        if ($link) {
          $rows [] = array ("<a href='$link'>". $name . '</a>');  
        }
        else {
          $rows [] = array ($name);
        }
    }
    $header = array ('Name');
    $table = array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array(
            'id' => 'tripal_feature-table-trait',
        ),
        'sticky' => FALSE,
        'caption' => '',
        'colgroups' => array(),
        'empty' => '',
    );
    print theme_table($table);
}
?>