<?php

$project = $variables['node']->project;
$total_features = $project->num_qtl;
$num_per_page = 25;
$pager = mainlab_tripal_get_pager($total_features, $num_per_page, 'QTL');
$feature_positions = mainlab_tripal_get_project_QTL($project->project_id, $num_per_page, $pager['page'] - 1);
$all_positions = mainlab_tripal_get_project_QTL($project->project_id, 'ALL', 0);


if($total_features > 0){ ?>
  <div class="tripal_project-data-block-desc tripal-data-block-desc" style="float:left">This project contains <?php print number_format($total_features) ?> QTL:</div> 
  <?php
  // Preparing download
  $dir = 'sites/default/files/tripal/mainlab_tripal/download';
  if (!file_exists($dir)) {
      mkdir ($dir, 0777, TRUE);
  }
  $download = $dir . '/project_qtl_data_project_id_' . $project->project_id . '.csv';
  $handle = fopen($download, "w");
  fwrite($handle, "QTL for Map: " . $project->name. "\n");
  fwrite($handle, '"#","Linkage Group","Name","Type","Start","Stop","Peak"' . "\n");
  $counter = 0;
  foreach ($all_positions as $position){
      $pos = $position->position || $position->position === '0' ? round($position->position, 2) . ' ' . $project->unittype_id->name : '-';
      $stop = $position->stop || $position->stop === '0' ? round($position->stop, 2) . ' ' . $project->unittype_id->name : '-';
      $peak = $position->peak || $position->peak === '0' ? round($position->peak, 2) . ' ' . $project->unittype_id->name : '-';
      fwrite($handle, '"' . ($counter + 1) . '","'. $position->lg . '","' . $position->marker . '","' . $position->type . '","' . $pos . '","' . $stop . '","' . $peak . '"' . "\n");
      $counter ++;
  }
  fclose($handle);
  ?>
  <div style="float: right">Download <a href="<?php print '/' . $download;?>">Table</a></div>
  <?php  
  // the $headers array is an array of fields to use as the colum headers.
  // additional documentation can be found here
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $headers = array('Linkage Group', 'Name', 'Type', 'Start', 'Stop', 'Peak');
  
  // the $rows array contains an array of rows where each row is an array
  // of values for each column of the table in that row.  Additional documentation
  // can be found here:
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $rows = array();
  
  foreach ($feature_positions as $position){
    $link = mainlab_tripal_link_record('feature', $position->feature_id);
    if (!$link) {
      $link = mainlab_tripal_link_record ('feature', $position->marker_feature_id);
    }
    $rows[] = array(
      $position->lg,
      $link ? "<a href='$link'>" . $position->marker . '</a>': $position->marker,
      $position->type,
      $position->position || $position->position === '0' ? round($position->position, 2) . ' ' . $project->unittype_id->name : '-',
      $position->stop || $position->stop === '0' ? round($position->stop, 2) . ' ' . $project->unittype_id->name : '-',
      $position->peak || $position->peak === '0' ? round($position->peak, 2) . ' ' . $project->unittype_id->name : '-'
    );
  } 
  // the $table array contains the headers and rows array as well as other
  // options for controlling the display of the table.  Additional
  // documentation can be found here:
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $table = array(
    'header' => $headers,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_project-table-featurepos',
      'class' => 'tripal-data-table'
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  
  // once we have our table array structure defined, we call Drupal's theme_table()
  // function to generate the table.
  print theme_table($table);

  // Add pager
  print $pager['pager'];
}

