<?php
$project = $variables['node']->project;
$total_stocks = $project->num_stocks;
$num_per_page = 25;
$pager = mainlab_tripal_get_pager($total_stocks, $num_per_page, 'stocks');
$project_stocks = mainlab_tripal_get_project_stocks($project->project_id, $num_per_page, $pager['page'] - 1);

if($total_stocks > 0){ ?>
  <div class="tripal_project-data-block-desc tripal-data-block-desc" style="float:left">This project contains <?php print number_format($total_stocks) ?> stocks:</div> 
  <?php  
  // the $headers array is an array of fields to use as the colum headers.
  // additional documentation can be found here
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $headers = array('Name', 'Type');
  
  // the $rows array contains an array of rows where each row is an array
  // of values for each column of the table in that row.  Additional documentation
  // can be found here:
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $rows = array();
  
  foreach ($project_stocks as $stk){
    $link = mainlab_tripal_link_record('stock', $stk->stock_id);
    $rows[] = array(
      $link ? "<a href='$link'>" . $stk->name . '</a>': $stk->name,
      $stk->type
    );
  } 
  // the $table array contains the headers and rows array as well as other
  // options for controlling the display of the table.  Additional
  // documentation can be found here:
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $table = array(
    'header' => $headers,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_project-table-stocks',
      'class' => 'tripal-data-table'
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  
  // once we have our table array structure defined, we call Drupal's theme_table()
  // function to generate the table.
  print theme_table($table);

  // Add pager
  print $pager['pager'];
}

