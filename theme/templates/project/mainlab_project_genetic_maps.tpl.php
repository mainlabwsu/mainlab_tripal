<?php

$project = $variables['node']->project;
$total_maps = $project->num_maps;
$num_per_page = 25;
$pager = mainlab_tripal_get_pager($total_maps, $num_per_page, 'maps');
$maps = mainlab_tripal_get_project_genetic_maps($project->project_id, $num_per_page, $pager['page'] - 1);

if($total_maps > 0){ ?>
  <div class="tripal_project-data-block-desc tripal-data-block-desc" style="float:left">This project contains <?php print number_format($total_maps) ?> Maps:</div> 
  <?php  
  // the $headers array is an array of fields to use as the colum headers.
  // additional documentation can be found here
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $headers = array('Name');
  
  // the $rows array contains an array of rows where each row is an array
  // of values for each column of the table in that row.  Additional documentation
  // can be found here:
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $rows = array();
  
  foreach ($maps as $map){
    $link = mainlab_tripal_link_record('featuremap', $map->featuremap_id);
    $rows[] = array(
      $link ? '<a href=' . $link . '>' . $map->name . '</a>' : $map->name,
    );
  } 
  // the $table array contains the headers and rows array as well as other
  // options for controlling the display of the table.  Additional
  // documentation can be found here:
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $table = array(
    'header' => $headers,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_project-table-project-maps',
      'class' => 'tripal-data-table'
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  
  // once we have our table array structure defined, we call Drupal's theme_table()
  // function to generate the table.
  print theme_table($table);
  
  // Add pager
  print $pager['pager'];
}

