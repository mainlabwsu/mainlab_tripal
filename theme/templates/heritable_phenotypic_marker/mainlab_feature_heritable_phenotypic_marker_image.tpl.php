<?php
$feature = $variables['node']->feature;
$images = $feature->images;

if (mainlab_tripal_count($images) > 0) {
    $rows = array();
    foreach ($images AS $img) {
        $link = mainlab_tripal_link_record('eimage', $img->eimage_id);
        if ($link) {
            $rows [] = array ("<a href='$link' style='white-space:nowrap'>". $img->image_uri . '</a>', $img->legend);
        }
        else {
            $rows [] = array ($img->image_uri, $img->legend);
        }
    }
    $header = array ('File Name', 'Legend');
    $table = array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array(
            'id' => 'tripal_feature-table-image',
        ),
        'sticky' => FALSE,
        'caption' => '',
        'colgroups' => array(),
        'empty' => '',
    );
    print theme_table($table);
} 