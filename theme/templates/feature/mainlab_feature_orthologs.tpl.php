<?php
$feature = $variables['node']->feature;
$synblocks = mainlab_tripal_feature_get_synblocks($feature->feature_id);
$orthologs = mainlab_tripal_feature_get_orthologs($feature->feature_id);
$paralogs = mainlab_tripal_feature_get_paralogs($feature->feature_id);

if (mainlab_tripal_count($synblocks) > 0) {
  $header = array ('Syntentic block', 'Assembly', 'Species');

  $rows = array ();
  $counter = 1;

  foreach($synblocks AS $b) {
    $f = "<a href=\"/synview/block/" . preg_replace("/(.+)R|L(\d+)/", "$1B$2", $b->uniquename) . "\">$b->uniquename</a>";

    $alink = mainlab_tripal_link_record('analysis', $b->assembly_id);
    $a = $alink ? "<a href=\"$alink\">$b->assembly</a>" : $b->assembly;

    $olink = mainlab_tripal_link_record('organism', $b->organism_id);
    $o = $olink ? "<a href=\"$olink\">$b->organism</a>" : $b->organism;

    $rows[] = array ($f, $a, $o);

    $counter ++;
  }
  $table = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_feature_genetic_marker-table-map-positions',
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  print "<br><div>Syntenic blocks</div>";
  print theme_table($table);
}

if (mainlab_tripal_count($orthologs) > 0) {
  $header = array ('Gene/Transcript', 'Assembly', 'Species');

  $rows = array ();
  $counter = 1;

  foreach($orthologs AS $o) {
    $flink = mainlab_tripal_link_record('feature', $o->feature_id);
    $f = $flink ? "<a href=\"$flink\">$o->name</a>" : $o->name;

    $alink = mainlab_tripal_link_record('analysis', $o->assembly_id);
    $a = $alink ? "<a href=\"$alink\">$o->assembly</a>" : $o->assembly;

    $olink = mainlab_tripal_link_record('organism', $o->organism_id);
    $o = $olink ? "<a href=\"$olink\">$o->organism</a>" : $o->organism;

    $rows[] = array ($f, $a, $o);

    $counter ++;
  }
  $table = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_feature_genetic_marker-table-map-positions',
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  print "<br><div>Orthologs</div>";
  print theme_table($table);
}

if (mainlab_tripal_count($paralogs) > 0) {
  $header = array ('Gene/Transcript', 'Assembly', 'Species');

  $rows = array ();
  $counter = 1;

  foreach($paralogs AS $p) {
    $flink = mainlab_tripal_link_record('feature', $p->feature_id);
    $f = $flink ? "<a href=\"$flink\">$p->name</a>" : $p->name;

    $alink = mainlab_tripal_link_record('analysis', $p->assembly_id);
    $a = $alink ? "<a href=\"$alink\">$p->assembly</a>" : $p->assembly;

    $plink = mainlab_tripal_link_record('organism', $p->organism_id);
    $p = $plink ? "<a href=\"$plink\">$p->organism</a>" : $p->organism;

    $rows[] = array ($f, $a, $p);

    $counter ++;
  }
  $table = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_feature_genetic_marker-table-map-positions',
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  print "<br><div>Gene/transcripts from the same species that appear to represent the same gene</div>";
  print theme_table($table);
}
?>

