<?php
$feature = $variables['node']->feature;
$pangenset = mainlab_tripal_get_pan_genes($feature->feature_id);

// don't show this page if there are no analyses
if (mainlab_tripal_count($pangenset) > 0) { ?>
  <div class="tripal_feature-data-block-desc tripal-data-block-desc">This <?php print $feature->type_id->name ?> belongs to the following pan gene set:</div> <?php
  foreach ($pangenset AS $pset) {

    print "Pan Gene Set: <b>" . $pset['name'] . "</b>";

    $headers = array('Gene Model' ,'Assembly', 'Computed By');

    $rows = array();

    $analysis_name = $pset['analysis'];
    $link = mainlab_tripal_link_record('analysis', $pset['analysis_id']);
    if ($link) {
      $analysis_name = l($analysis_name, $link);
    }

    foreach ($pset['pan_genes'] as $feature_id => $pgene) {

      $assembly = $pgene['analysis'];
      $alink = mainlab_tripal_link_record('analysis', $pgene['analysis_id']);
      if ($alink) {
        $assembly = l($assembly, $alink);
      }

      $gmodel = $pgene['name'];
      $flink = mainlab_tripal_link_record('feature', $feature_id);
      if ($flink) {
        $gmodel = l($gmodel, $flink);
      }

      $rows[] = array(
        $gmodel,
        $assembly,
        $analysis_name
      );
    }

    $table = array(
      'header' => $headers,
      'rows' => $rows,
      'attributes' => array(
        'id' => 'tripal_feature-table-analyses',
        'class' => 'tripal-data-table'
      ),
      'sticky' => FALSE,
      'caption' => '',
      'colgroups' => array(),
      'empty' => '',
    );

    print theme_table($table);
  }
}

