<?php
$stock = $node->stock;
$organism = $node->stock->organism_id;
$main_db_reference = $stock->dbxref_id;

// synonyms are stored in the stockprop table with a type of 'synonym'  or 'alias'
$stock->stock_synonyms = chado_generate_var(
    'stockprop',
    array(
        'stock_id'=> $stock->stock_id,
        'type_id' => array(
            'cv_id' => array('name' => 'feature_property'),
            'name'  => 'synonym'
        ),
    )
);

// Prepare synomyns data with type 'alias'
$synonyms = $stock->stock_synonyms;
if (!$synonyms) {
  $stock->stock_synonyms = chado_generate_var(
      'stockprop',
      array(
          'stock_id'=> $stock->stock_id,
          'type_id' => array(
              'cv_id' => array('name' => 'MAIN'),
              'name'  => 'alias'
          ),
      )
  );
  $synonyms = $stock->stock_synonyms;
}
$num_synonyms = is_array($synonyms) ? mainlab_tripal_count($synonyms) : 0;
$syn = "";
if ($num_synonyms == 0) {
  $syn = "N/A";
} else {
  if (is_array($synonyms)) {
     $syn = $synonyms[0]->value . " [<a href=\"?pane=alias\">view all $num_synonyms</a>]";
  } else {
    $syn = $synonyms->value;
  }
}

// Institutional Name
$iname = NULL;
if (isset($stock->institutional_name)) {
  foreach($stock->institutional_name AS $in) {
    $iname .= $in . '<br>';
  }
}

// Sample Name
$sname = NULL;
if (isset($stock->sample_name)) {
  foreach($stock->sample_name AS $sn) {
    $sname .= $sn . '<br>';
  }
}

// GRIN ID
$grin = "";
/*
$in_collection = $stock->in_collection;
foreach ($in_collection as $coll){
  if ($coll->db == 'GRIN') {
    $grin .= "<a href=\"https://npgsweb.ars-grin.gov/gringlobal/accessiondetail?accid=". $coll->accession . "\" target=_blank>" . $coll->accession . "</a><br>";
  }
}
*/
$grin_id = $stock->grin_id;
foreach ($grin_id as $g){
  $grin .= "<a href=\"https://npgsweb.ars-grin.gov/gringlobal/accessiondetail?accid=". $g . "\" target=_blank>" . $g . "</a><br>";
}

// Prepare properties data
$stock = chado_expand_var($stock, 'table', 'stockprop', array('return_array' => 1));
$properties = $stock->stockprop;
$properties = chado_expand_var($properties, 'field', 'stockprop.value');
$desc = "N/A";
$orig = "N/A";
$orig_country = "N/A";
$pedigree = "N/A";
$comment = "N/A";
$reference = "N/A";

if ($properties) {
  foreach ( $properties as $prop ) {
    if ($prop->type_id->name == 'description') {
      if ($desc == "N/A") {
        $desc = $prop->value;
      }
      else {
        $desc .= ". " . $prop->value;
      }
    }
    elseif ($prop->type_id->name == 'origin_detail') {
      $orig = $prop->value;
    }
    elseif ($prop->type_id->name == 'origin_country' || $prop->type_id->name == 'origin') {
      $orig_country = $prop->value;
    }
    elseif ($prop->type_id->name == 'pedigree') {
      $pedigree = $prop->value;
    }
    elseif ($prop->type_id->name == 'comments') {
      $comment = $prop->value;
    }
    elseif ($prop->type_id->name == 'reference') {
      $reference = $prop->value;
    }
  }
}

if ($reference == 'N/A') {
  $num_pub = chado_query("SELECT count (*) FROM {stock_pub} WHERE stock_id = :stock_id", array(':stock_id' => $stock->stock_id))->fetchField();
  if ($num_pub > 1) {
    $reference = '[<a href="?pane=publications">view all ' . $num_pub . '</a>]';
  }
  else if ($num_pub == 1) {
    $pub_id = chado_query("SELECT pub_id FROM {stock_pub} WHERE stock_id = :stock_id", array(':stock_id' => $stock->stock_id))->fetchField();
    $plink = mainlab_tripal_link_record('pub', $pub_id);
    $citation = chado_query("SELECT uniquename FROM {pub} WHERE pub_id = :pub_id", array(':pub_id' => $pub_id))->fetchField();
    $reference = l($citation, $plink ,array('attributes' => array('target' => '_blank')));
  }
}
$num_seq = chado_query("SELECT count (*) FROM {feature_stock} WHERE stock_id = :stock_id", array(':stock_id' => $stock->stock_id))->fetchField();

$stock_type = ucwords(preg_replace('/_/', ' ', $stock->type_id->name));
if($stock_type == 'TBD'){
  $stock_type = 'undefined';
}
else if ($stock_type == 'Accession') {
  $subtype = db_query(
      "SELECT value FROM chado.stockprop 
       WHERE type_id = 
        (SELECT cvterm_id FROM chado.cvterm 
         WHERE name = 'accession_type' 
         AND cv_id = 
           (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')
        ) 
       AND stock_id = :stock_id", 
      [':stock_id' => $stock->stock_id])->fetchField();
      $stock_type = $subtype ? ucwords(preg_replace('/_/', ' ', $subtype)) : $stock_type;
}

// Maternal parents
$maternal_parent_of = $stock->maternal_parent_of;
$num_mparent_of = is_array($maternal_parent_of) ? mainlab_tripal_count($maternal_parent_of) : 0;
$first_mparent_of = "N/A";
if ($num_mparent_of > 0) {
  $link_mpo = mainlab_tripal_link_record('stock', $maternal_parent_of[0]->stock_id);
  $first_mparent_of = l($maternal_parent_of[0]->uniquename, $link_mpo);
  if ($num_mparent_of > 1) {
    $first_mparent_of .= " [<a href=\"?pane=maternal_parent_of\">view all " . $num_mparent_of . "</a>]";
  }
}

//Paternal parents
$paternal_parent_of = $stock->paternal_parent_of;
$num_pparent_of = is_array($paternal_parent_of) ? mainlab_tripal_count($paternal_parent_of) : 0;
$first_pparent_of = "N/A";
if ($num_pparent_of > 0) {
  $link_ppo = mainlab_tripal_link_record('stock', $paternal_parent_of[0]->stock_id);
  $first_pparent_of = l($paternal_parent_of[0]->uniquename, $link_ppo);
  if ($num_pparent_of > 1) {
    $first_pparent_of .= " [<a href=\"?pane=paternal_parent_of\">view all " . $num_pparent_of . "</a>]";
  }
}

// Population Maps
$num_population_map =  property_exists($stock, 'population_map') ? mainlab_tripal_count($stock->population_map) : 0;

// Phenotypic Data
$num_phenotypic_data =  property_exists($stock, 'phenotypic_data') ? mainlab_tripal_count($stock->phenotypic_data) : 0;

// SSR Genotype Data
$num_genotypic_data =  property_exists($stock, 'genotypic_data') ? mainlab_tripal_count($stock->genotypic_data) : 0;

// SNP Genotype Data
$num_snp_data =  property_exists($stock, 'genotypic_data') ? mainlab_tripal_count($stock->snp_genotype_data) : 0;

// Library
$stock = db_table_exists('library_stock') ? chado_expand_var($stock, 'table', 'library_stock', array('return_array' => 1)) : $stock;
$num_libraries = isset($stock->library_stock) && property_exists($stock, 'library_stock') ? mainlab_tripal_count($stock->library_stock) : 0;

$headers = array();
$rows = array();
$rows [] = array(array('data' => 'Name', 'header' => TRUE, 'width' => '20%'), $stock->uniquename);
$rows [] = array(array('data' => 'Alias', 'header' => TRUE, 'width' => '20%'), $syn);
if ($iname) {
  $rows [] = array(array('data' => 'Institutional Name', 'header' => TRUE, 'width' => '20%'), $iname);
}
if ($sname) {
  $rows [] = array(array('data' => 'Sample Name', 'header' => TRUE, 'width' => '20%'), $sname);
}
$rows [] = array(array('data' => 'GRIN ID', 'header' => TRUE, 'width' => '20%'), $grin ? $grin : 'N/A');
$rows [] = array(array('data' => 'Type', 'header' => TRUE, 'width' => '20%'), $stock_type);
$link = mainlab_tripal_link_record('organism', $organism->organism_id);
$rows [] = array(array('data' => 'Species', 'header' => TRUE, 'width' => '20%'), $link ? "<a href=\"$link\">".$organism->genus ." " . $organism->species . "</a>" : $organism->genus ." " . $organism->species);
if (isset($stock->in_collection) && mainlab_tripal_count($stock->in_collection) > 0) {
  $rows [] = array(array('data' => 'In Collection', 'header' => TRUE, 'width' => '20%'), "[<a href=\"?pane=in_collection\">view all</a>]");
}
$rows [] = array(array('data' => 'Description', 'header' => TRUE, 'width' => '20%'), $desc);
$rows [] = array(array('data' => 'Origin Country', 'header' => TRUE, 'width' => '20%'), $orig_country);
$rows [] = array(array('data' => 'Origin Detail', 'header' => TRUE, 'width' => '20%'), $orig);
$rows [] = array(array('data' => 'Pedigree', 'header' => TRUE, 'width' => '20%'), $pedigree);
$mplink = mainlab_tripal_link_record('stock', $stock->maternal_parent->stock_id);
$pplink = mainlab_tripal_link_record('stock', $stock->paternal_parent->stock_id);
$mp = isset($stock->maternal_parent->uniquename)? l($stock->maternal_parent->uniquename, $mplink) : 'N/A';
$pp = isset($stock->paternal_parent->uniquename)? l($stock->paternal_parent->uniquename, $pplink) : 'N/A';
$rows [] = array(array('data' => 'Maternal Parent', 'header' => TRUE, 'width' => '20%'), $mp);
$rows [] = array(array('data' => 'Paternal Parent', 'header' => TRUE, 'width' => '20%'), $pp);
$rows [] = array(array('data' => 'Maternal Parent of', 'header' => TRUE, 'width' => '20%'), $first_mparent_of);
$rows [] = array(array('data' => 'Paternal Parent of', 'header' => TRUE, 'width' => '20%'), $first_pparent_of);
$rows [] = array(array('data' => 'Phenotypic Data', 'header' => TRUE, 'width' => '20%'), $num_phenotypic_data > 0 ? "[<a href='?pane=phenotypic_data'>view all $num_phenotypic_data</a>]" : 'N/A');
$rows [] = array(array('data' => 'SSR Genotype Data', 'header' => TRUE, 'width' => '20%'), $num_genotypic_data > 0 ? "[<a href='?pane=ssr_genotype_data'>view all $num_genotypic_data</a>]" : 'N/A');
$rows [] = array(array('data' => 'SNP Genotype Data', 'header' => TRUE, 'width' => '20%'), $num_snp_data > 0 ? "[<a href='?pane=snp_genotype_data'>view all $num_snp_data</a>]" : 'N/A');
$rows [] = array(array('data' => 'Map', 'header' => TRUE, 'width' => '20%'), $num_population_map > 0 ? "[<a href='?pane=genetic_map_data'>view all $num_population_map</a>]" : 'N/A');
$rows [] = array(array('data' => 'DNA Library', 'header' => TRUE, 'width' => '20%'), $num_libraries > 0 ? "[<a href='?pane=library'>view all $num_libraries </a>]" : 'N/A');
$rows [] = array(array('data' => 'Sequence', 'header' => TRUE, 'width' => '20%'), $num_seq > 0 ? "[<a href=\"/feature_listing/_/_/$stock->uniquename\">view all $num_seq </a>]" : 'N/A');
$rows [] = array(array('data' => 'Comments', 'header' => TRUE, 'width' => '20%'), $comment);
$rows [] = array(array('data' => 'Reference', 'header' => TRUE, 'width' => '20%'), $reference);

// allow site admins to see the feature ID
if (user_access('view ids') || user_access('view chado_ids')) {
  $rows[] = array(array('data' => 'Stock ID', 'header' => TRUE, 'class' => 'tripal-site-admin-only-table-row'), array('data' => $stock->stock_id, 'class' => 'tripal-site-admin-only-table-row'));
}
$table = array(
  'header' => $headers,
  'rows' => $rows,
  'attributes' => array(
    'id' => 'tripal_stock-table-custom_base',
  ),
  'sticky' => FALSE,
  'caption' => '',
  'colgroups' => array(),
  'empty' => '',
);
print theme_table($table);
?>

