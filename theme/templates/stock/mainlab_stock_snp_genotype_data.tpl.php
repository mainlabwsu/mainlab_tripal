<?php
$stock = $variables['node']->stock;
$all_records = $stock->snp_genotype_data;
$total = mainlab_tripal_count($all_records);
$num_per_page = 25;
$pager = mainlab_tripal_get_pager($total, $num_per_page, 'snp_genotype_data');
$records = mainlab_tripal_get_stock_snp_genotype($stock->stock_id, $num_per_page, $pager['page'] - 1);

// get the total number of records
if($total > 0){ ?>
  <div class="tripal_stock-data-block-desc tripal-data-block-desc" style="float:left">Total <?php print number_format($total) ?> records</div>
  <?php
  // Preparing download

  $dir = 'sites/default/files/tripal/mainlab_tripal/download';
  if (!file_exists($dir)) {
      mkdir ($dir, 0777, TRUE);
  }
  $download = $dir . '/stock_snp_genotype_stock_id_' . $stock->stock_id . '.csv';
  $handle = fopen($download, "w");
  fwrite($handle, "SNP genotype for stock: " . $stock->name. "\n");
  fwrite($handle, '"#","Marker Name","Allele","Genotype"' . "\n");
  $counter = 0;
  foreach ($all_records as $rec){
      fwrite($handle, '"' . ($counter + 1) . '","'. $rec->feature_name . '","' . $rec->allele . '","' . $rec->genotype . '"' . "\n");
      $counter ++;
  }
  fclose($handle);
  ?>
  <div style="float: right">Download <a href="<?php print '/' . $download;?>">Table</a></div>
  <style>
  /* Change Snp to SNP */
  #snp_genotype_data, .snp_genotype_data-tripal-data-pane-title {
    visibility: hidden;
    position: relative;
  }
  #snp_genotype_data::after, .snp_genotype_data-tripal-data-pane-title::after {
    content: 'SNP Genotype Data';
    visibility: visible;
    position: absolute;
    left:0;
  }
  </style>

  <?php

  $headers = array('#', 'Dataset', 'Marker Name', 'Allele', 'Genotype');

  $rows = array();
  $counter = 1;
  foreach ($records as $rec){
    $link = mainlab_tripal_link_record('feature', $rec->feature_id);
    $plink = mainlab_tripal_link_record('project', $rec->project_id);
    $rows[] = array(
      $counter,
      $plink ? "<a href='$plink'>" . $rec->project_name . '</a>': $rec->project_name,
      $link ? "<a href='$link'>" . $rec->feature_name . '</a>': $rec->feature_name,
      $rec->allele,
      $rec->genotype,
    );
    $counter ++;
  }

  $table = array(
    'header' => $headers,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_stock-table-featurepos',
      'class' => 'tripal-data-table'
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );

  print theme_table($table);
  print $pager['pager'];
}

