<?php
$stock = $variables['node']->stock;
$all_records = $stock->qtl;
$total = mainlab_tripal_count($all_records);
$num_per_page = 25;
$pager = mainlab_tripal_get_pager($total, $num_per_page, 'qtl');
$records = mainlab_tripal_get_stock_qtl($stock->stock_id, $num_per_page, $pager['page'] - 1);

// get the total number of records
if($total > 0){ ?>
  <div class="tripal_stock-data-block-desc tripal-data-block-desc" style="float:left">Total <?php print number_format($total) ?> records</div>
  
    <style>
  /* Change Snp to SNP */
  #qtl, .qtl-tripal-data-pane-title {
    visibility: hidden;
    position: relative;
  }
  #qtl::after, .qtl-tripal-data-pane-title::after {
    content: 'QTL';
    visibility: visible;
    position: absolute;
    left:0;
  }
  </style>
  
  <?php

  $headers = array('#', 'Name', 'Dataset');

  $rows = array();
  $counter = 1;
  foreach ($records as $rec){
    $link = mainlab_tripal_link_record('feature', $rec->feature_id);
    $jlink = mainlab_tripal_link_record('project', $rec->project_id);
    $rows[] = array(
      $counter,
      $link ? "<a href='$link'>" . $rec->uniquename . '</a>': $rec->uniquename,
      $jlink ? "<a href='$jlink'>" . $rec->project . '</a>': $rec->project,
    );
    $counter ++;
  }

  $table = array(
    'header' => $headers,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_stock-table-featurepos',
      'class' => 'tripal-data-table'
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );

  print theme_table($table);
  print $pager['pager'];
}

