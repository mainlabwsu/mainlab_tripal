<?php
$feature = $variables['node']->feature;
$locations = $feature->mainlab_gwas->locations;
$counter_loc = mainlab_tripal_count($locations);

if ($counter_loc > 0) {
  $header = module_exists('tripal_map') ? array ('Name', 'Type', 'Location', 'Analysis', 'MapViewer') : array ('Name', 'Type', 'Location', 'Analysis');
  $rows = array ();
  foreach($locations AS $loc) {
    $lm = $loc->landmark;
    $location = $lm . ':' . ($loc->fmin + 1) . '..' . $loc->fmax;
    $alink = mainlab_tripal_link_record('analysis', $loc->analysis_id);
    $analysis = $alink ? '<a href=' . $alink . '>' . $loc->analysis . '</a>' : $loc->analysis;
    if ($loc->jbrowse_url) {
      $lm = '<a href=' . $loc->jbrowse_url . $lm . '>' . $lm . '</a>';
      $location = '<a href=' . $loc->jbrowse_url . $location . '>' . $location . '</a>';
    }
    $rows[] = module_exists('tripal_map') ? array ($lm, $loc->type, $location, $analysis, '<a href="/mapviewer_gwas/' . $loc->analysis_id . '/' . $loc->srcfeature_id . '/' . $feature->feature_id . '">View</a>') : array ($lm, $loc->type, $location, $analysis);
  }
  $table = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_feature_qtl-table-genome-loccation',
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  print theme_table($table);
} ?>
