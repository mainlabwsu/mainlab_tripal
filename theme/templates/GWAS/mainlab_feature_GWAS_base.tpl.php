<?php

$feature  = $variables['node']->feature;
$feature = chado_expand_var($feature,'table','featureprop', array('return_array' => 1));

$variables['node']->title = $feature->uniquename;

$title = db_query("SELECT title FROM tripal_entity WHERE id = :id", [':id' => 1]);
// Process featureprop (i.e. Triait Symbol / LOD / R2 / AD ratio / Comments
$properties = $feature->featureprop;

$symbol   = "N/A";
$LOD      = "N/A";
$R2       = "N/A";
$p_value      = "N/A";
$comments = "N/A";
$stat_method = "N/A";
$exp_model = "N/A";
$other_props = [];
if ($properties) {
  foreach ($properties AS $prop) {
    if ($prop->type_id->name == 'published_symbol') {
      $symbol = $prop->value;
    }
    else if ($prop->type_id->name == 'LOD') {
      $LOD = is_numeric($prop->value) ? round($prop->value, 2) : $prop->value;
    }
    else if ($prop->type_id->name == 'R2') {
      $R2 = is_numeric($prop->value) ? round($prop->value, 2) : $prop->value;
    }
    else if ($prop->type_id->name == 'p_value') {
      $p_value = $prop->value;
    }
    else if ($prop->type_id->cv_id->name == 'MAIN' && $prop->type_id->name == 'comments') {
      $comments = $prop->value;
    }
    else if ($prop->type_id->cv_id->name == 'MAIN' && $prop->type_id->name == 'statistical_method') {
      $stat_method = $prop->value;
    }
    else if ($prop->type_id->cv_id->name == 'MAIN' && $prop->type_id->name == 'experimental_model') {
      $exp_model = $prop->value;
    }
    else {
      $other_props[$prop->type_id->name] = $prop->value;
    }
  }
}

// Generate GWAS details
$gwas_details = $feature->mainlab_gwas->gwas_details;
$gwas_locs = $feature->mainlab_gwas->locations;

// Synonyms
$synonyms = "N/A";
if ($gwas_details->synonyms) {
  $synonyms = "";
  foreach ($gwas_details->synonyms as $syn) {
    $synonyms .= $syn->name . ". ";
  }
}

// GWAS Marks
$gwasMarker = "N/A";
if (mainlab_tripal_count($gwas_details->gwas_marker) != 0) {
  $gwasMarker = "";
}
if ($gwas_details->gwas_marker) {
  foreach($gwas_details->gwas_marker as $marker) {
    $nlink = mainlab_tripal_link_record('feature', $marker->feature_id);
    $gwasMarker .= "<a href=\"$nlink\">" . $marker->gwas_marker . "</a><br>";
  }
}
$headers = array();
$rows = array();
$srcfeature_id = NULL;
foreach ($gwas_locs AS $loc) {
  $srcfeature_id = $loc->analysis_id == $gwas_details->genome->analysis_id ? $loc->srcfeature_id : NULL;
  if ($srcfeature_id) {
    break;
  }
}
$mapviewer = module_exists('tripal_map') && $srcfeature_id ? ' [<a href="/mapviewer_gwas/' . $gwas_details->genome->analysis_id . '/' . $srcfeature_id . '/' . $feature->feature_id . '"> MapViewer </a>]' : '';
$rows [] = array(array('data' => 'GWAS Label', 'header' => TRUE, 'width' => '20%'), $feature->uniquename . $mapviewer);
$rows [] = array(array('data' => 'Published Symbol', 'header' => TRUE, 'width' => '20%'), $symbol);
$trait = "N/A";
if (!empty($gwas_details->trait)) {
  $trait = '<a href="/trait/' . $gwas_details->trait->cvterm_id . '">' . $gwas_details->trait->name . '</a>';
}
$rows [] = array(array('data' => 'Trait Name', 'header' => TRUE, 'width' => '20%'), $trait);
$rows [] = array(array('data' => 'Trait Alias', 'header' => TRUE, 'width' => '20%'), $synonyms);
$studylink = mainlab_tripal_link_record('project', $gwas_details->study_id);
$study = $studylink? '<a href="' . $studylink . '">' . $gwas_details->study . '</a>' : $gwas_details->study;
$rows [] = array(array('data' => 'GWAS Marker', 'header' => TRUE, 'width' => '20%'), $gwasMarker);
$rows [] = array(array('data' => 'GWAS Study', 'header' => TRUE, 'width' => '20%'), $study);
$genomelink = mainlab_tripal_link_record('analysis', $gwas_details->genome->analysis_id);
$genome = $genomelink ? '<a href="' . $genomelink . '">' . $gwas_details->genome->name . '</a>' : $gwas_details->genome->name;
$rows [] = array(array('data' => 'Genome Assembly', 'header' => TRUE, 'width' => '20%'), $genome);
$rows [] = array(array('data' => 'Statistical Mothod', 'header' => TRUE, 'width' => '20%'), $stat_method);
$rows [] = array(array('data' => 'Experimental Model', 'header' => TRUE, 'width' => '20%'), $exp_model);
$panel = "N/A";
if (!empty($gwas_details->panel)) {
  $panellink = mainlab_tripal_link_record('stock', $gwas_details->panel->stock_id);
  $panel = $panellink? '<a href="' . $panellink . '">' . $gwas_details->panel->uniquename . '</a>' : $gwas_details->panel->uniquename;
}
$rows [] = array(array('data' => 'GWAS Panel', 'header' => TRUE, 'width' => '20%'), $panel);
$rows [] = array(array('data' => 'LOD', 'header' => TRUE, 'width' => '20%'), $LOD);
$rows [] = array(array('data' => 'P Value', 'header' => TRUE, 'width' => '20%'), $p_value);
$rows [] = array(array('data' => 'R2', 'header' => TRUE, 'width' => '20%'), $R2);
foreach ($other_props AS $k => $v) {
  $rows [] = array(array('data' => ucfirst(str_replace('_', ' ', $k)), 'header' => TRUE, 'width' => '20%'), $v);
}
$rows [] = array(array('data' => 'Comments', 'header' => TRUE, 'width' => '20%'), $comments);
// allow site admins to see the feature ID
if (user_access('view ids') || user_access('view chado_ids')) {
  $rows[] = array(array('data' => 'Feature ID', 'header' => TRUE, 'class' => 'tripal-site-admin-only-table-row'), array('data' => $feature->feature_id, 'class' => 'tripal-site-admin-only-table-row'));
}
$table = array(
  'header' => $headers,
  'rows' => $rows,
  'attributes' => array(
    'id' => 'tripal_feature_GWAS-table-base',
  ),
  'sticky' => FALSE,
  'caption' => '',
  'colgroups' => array(),
  'empty' => '',
);
print theme_table($table);
?>
