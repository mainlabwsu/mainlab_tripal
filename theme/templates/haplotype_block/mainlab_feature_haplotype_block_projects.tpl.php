<?php
$feature = $variables['node']->feature;
$projects = property_exists($feature, 'projects') ? $feature->projects : array();
$counter_projs = mainlab_tripal_count($projects);

 if ($counter_projs > 0) {

  $header = array ('Project Name');
  
  $rows = array ();
  $counter = 1; 

  foreach($projects AS $proj) {
    $link = mainlab_tripal_link_record('project', $proj->project_id);
    $project = $link ? "<a href=\"$link\">$proj->name</a>" : $proj->name;
    $rows[] = array ($project);
    $counter ++;
  }
  $table = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_feature_haplotype_block-table-projects',
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  print theme_table($table);
} ?>

