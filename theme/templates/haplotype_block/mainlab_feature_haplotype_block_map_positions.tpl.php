<?php
$feature = $variables['node']->feature;
$map_positions = property_exists($feature, 'map_positions') ? $feature->map_positions : array();
$counter_pos = mainlab_tripal_count($map_positions);
 if ($counter_pos > 0) {

  $header = array ('Haplotype Block', 'Map Name', 'Linkage Group', 'Start', 'Stop');
  
  $rows = array ();
  $counter = 1; 

  foreach($map_positions AS $pos) {
    $link = mainlab_tripal_link_record('featuremap', $pos->featuremap_id);
    $map = $link ? "<a href=\"$link\">$pos->map</a>" : $pos->map;
    $lg = $pos->linkage_group ? $pos->linkage_group : "N/A";
    $start = round($pos->start, 2);
    $stop = round($pos->stop, 2);
    $locus = $pos->name;
    $highlight = $node->feature->uniquename;

    $rows[] = array ($locus, $map, $lg, $start, $stop);

    $counter ++;
  }
  $table = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_feature_genetic_marker-table-map-positions',
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  print theme_table($table);
} ?>

