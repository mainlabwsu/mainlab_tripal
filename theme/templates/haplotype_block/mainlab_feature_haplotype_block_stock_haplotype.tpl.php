<?php
$feature = $variables['node']->feature;
$stocks = $feature->stocks;

if (mainlab_tripal_count($stocks) > 0) { ?>
  <div id="tripal_feature-stock_haplotypes-box" class="tripal_feature-info-box tripal-info-box">
    <table id="tripal_feature-stock_haplotypes-table" class="tripal_feature-table tripal-table tripal-table-horz" style="width:100%;">
      <tr><th style="width:40%">Name</th><th>Haplotype</th></tr>
    <?php
      $count = 1;
      foreach($stocks as $stock) {
        $link = mainlab_tripal_link_record('stock', $stock->stock_id);
        if ($count % 2 == 1) {
          $class = "odd";
        }
        else {
          $class = 'even';
        }
        print
        '<tr class="tripal_feature-table-' . $class . '-row ' . $class . '">
          <td><a href=' . $link . '>' . $stock->stock . '</a></td>
          <td>'. $stock->haplotype . '</td>
        </tr>';
        $count ++;
      }
    ?>
    </table>
  </div>
<?php } ?>