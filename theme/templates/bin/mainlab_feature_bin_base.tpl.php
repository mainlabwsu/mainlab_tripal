<?php

$feature  = $variables['node']->feature;
$name = $feature->name;
if (!$name) {
  $name = $feature->uniquename; // show uniquname if there is no name
}

$rows [] = array(array('data' => 'Name', 'header' => TRUE, 'width' => '20%'), $feature->name);

if ($feature->featuremap_id) {
  $mlink = mainlab_tripal_link_record('featuremap', $feature->featuremap_id);
  $rows [] = array(array('data' => 'Map', 'header' => TRUE, 'width' => '20%'), $mlink ? "<a href=\"$mlink\">".$feature->map . "</a>" : $feature->map);
}

if ($feature->lg) {
  $rows [] = array(array('data' => 'Linkage Group', 'header' => TRUE, 'width' => '20%'), $feature->lg);
}

if ($feature->start) {
  $rows [] = array(array('data' => 'Start', 'header' => TRUE, 'width' => '20%'), $feature->start);
}

if($feature->stop) {
  $rows [] = array(array('data' => 'Stop', 'header' => TRUE, 'width' => '20%'), $feature->stop);
}

// allow site admins to see the feature ID
if (user_access('view ids') || user_access('view chado_ids')) {
  $rows[] = array(array('data' => 'Feature ID', 'header' => TRUE, 'class' => 'tripal-site-admin-only-table-row'), array('data' => $feature->feature_id, 'class' => 'tripal-site-admin-only-table-row'));
}
$table = array(
  'header' => $headers,
  'rows' => $rows,
  'attributes' => array(
    'id' => 'tripal_feature_genetic_marker-table-base',
  ),
  'sticky' => FALSE,
  'caption' => '',
  'colgroups' => array(),
  'empty' => '',
);
print theme_table($table);
?>


