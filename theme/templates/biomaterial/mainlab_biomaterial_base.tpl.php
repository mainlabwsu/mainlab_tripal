<?php
$node = $variables['node'];
$biomaterial = $node->chado_record;
$biomaterial = chado_expand_var($biomaterial,'table','biomaterialprop', array('return_array' => 1));

$contact = $biomaterial->biosourceprovider_id;

$bprops = $biomaterial->biomaterialprop;
$props = array();
foreach($bprops AS $p) {
  $props[$p->type_id->name] = $p->value;
}
?>

    <div class="tripal_biomaterial-data-block-desc tripal-data-block-desc"></div> <?php

// the $headers array is an array of fields to use as the colum headers.
// additional documentation can be found here
// https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
// This table for the analysis has a vertical header (down the first column)
// so we do not provide headers here, but specify them in the $rows array below.
$headers = [];

// the $rows array contains an array of rows where each row is an array
// of values for each column of the table in that row.  Additional documentation
// can be found here:
// https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
$rows = [];

// Name row
$rows[] = [
  [
    'data' => 'Name',
    'header' => TRUE,
    'width' => '20%',
  ],
  $biomaterial->name,
];

// Organism row
$organism = $biomaterial->taxon_id->genus . " " . $biomaterial->taxon_id->species . " (" . $biomaterial->taxon_id->common_name . ")";
unset($props['organism']);
$link = mainlab_tripal_link_record('organism', $biomaterial->taxon_id->organism_id);
if ($link) {
  $organism = '<a href=' . $link . '>' . $organism . '</a>';
}
$rows[] = [
  [
    'data' => 'Organism',
    'header' => TRUE,
  ],
  $organism,
];

// Analysis row
if ($biomaterial->analysis) {
  $analysis = $biomaterial->analysis->analysis_name;
  $link = mainlab_tripal_link_record('analysis', $biomaterial->analysis->analysis_id);
  if ($link) {
    $analysis = '<a href=' . $link . '>' . $analysis . '</a>';
  }
  $rows[] = [
    [
      'data' => 'Analysis',
      'header' => TRUE,
    ],
    $analysis,
  ];
}

// Assays Date/Name
$collection_date = key_exists('collection_date', $props) ? $props['collection_date'] : 'n/a';
unset($props['collection_date']);
$rows[] = [
  [
    'data' => 'Collection Date',
    'header' => TRUE,
  ],
  $collection_date,
];

// Location
$location = key_exists('geo_loc_name', $props) ? $props['geo_loc_name'] : 'n/a';
unset($props['geo_loc_name']);
$rows[] = [
  [
    'data' => 'Location',
    'header' => TRUE,
  ],
  $location,
];

// Tissue
$tissue = key_exists('tissue', $props) ? $props['tissue'] : 'n/a';
unset($props['tissue']);
$rows[] = [
  [
    'data' => 'Tissue',
    'header' => TRUE,
  ],
  $tissue,
];

if ($biomaterial->description || $props['description']) {
  $desc = isset($props['description']) ? $props['description'] : $biomaterial->description;
  unset($props['description']);
  $rows[] = [
    [
      'data' => 'Description',
      'header' => TRUE,
    ],
    $desc,
  ];
}

$header_map = [
  'geo_loc_name' => 'Location',
  'growth_protocol' => 'Growth Protocol',
];
foreach ($props AS $key => $val) {
  if ($key == 'biosample_accession' || $key == 'biomaterial_accession') {
    $val = '<a href=https://www.ncbi.nlm.nih.gov/biosample/?term=' . $val . ' target=_blank>'. $val . '</a>';
  } else if ($key == 'sra_accession') {
    $val = '<a href=https://www.ncbi.nlm.nih.gov/sra/' . $val . ' target=_blank>'. $val . '</a>';
  }
  $rows[] = [
    [
      'data' => key_exists($key, $header_map) ? $header_map[$key] : str_replace('_', ' ', ucfirst($key)),
      'header' => TRUE,
    ],
    $val,
  ];
}

// Contact row
if (isset($contact->name)) {
  $link = mainlab_tripal_link_record('contact', $contact->contact_id);
  $contact_name = $link ? '<a href='. $link . '>' . $contact->name . '</a>' : $contact->name;
}
else {
  $contact_name = 'n/a';
}
$rows[] = [
  [
    'data' => 'Contact',
    'header' => TRUE,
    'width' => '20%',
  ],
  $contact_name,
];

// Assays
if ($assay_name) {
  $rows[] = [
    [
      'data' => 'Reference',
      'header' => TRUE,
    ],
    $assay_name == '' ? 'n/a' : $assay_name,
  ];
}

// allow site admins to see the biomaterial ID
if (user_access('view ids')) {
  // biomaterial ID
  $rows[] = [
    [
      'data' => 'biomaterial ID',
      'header' => TRUE,
      'class' => 'tripal-site-admin-only-table-row',
    ],
    [
      'data' => $biomaterial->biomaterial_id,
      'class' => 'tripal-site-admin-only-table-row',
    ],
  ];
}

// the $table array contains the headers and rows array as well as other
// options for controlling the display of the table.  Additional
// documentation can be found here:
// https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
$table = [
  'header' => $headers,
  'rows' => $rows,
  'attributes' => [
    'id' => 'tripal_biomaterial-table-base',
    'class' => 'tripal-data-table',
  ],
  'sticky' => FALSE,
  'caption' => '',
  'colgroups' => [],
  'empty' => '',
];

// once we have our table array structure defined, we call Drupal's theme_table()
// function to generate the table.
print theme_table($table);
