<?php
/*
 * Trait page
*/

function mainlab_trait_listing_form ($form, &$form_state) {
    $form = array();
    $form['label'] = array(
        '#markup' => '
          <div style="margin:20px 0px;">To see all the traits, click to expand the trait category below.  Or narrow the list using the keyword search.</div>          
          <div style="float:left;padding-top:4px;padding-right:10px;"><label>Keyword</label></div>
        ',
    );
    $form['keyword'] =array(
        '#id' => 'mainlab_trait_listing-id',
        '#type' => 'textfield',
        '#attributes' => array('style' => 'max-width:200px;float:left;margin-bottom:20px;margin-right:10px;'),
    );
    $form['submit'] = array(
        '#type' => 'button',
        '#value' => 'Search',
        '#ajax' => array(
            'callback' => 'mainlab_trait_listing_ajax_result',
            'wrapper' => 'mainlab_trait_listing-result-field',
            'effect' => 'fade'
        )
    );
    $result = '';
    if (!isset($form_state['values'])) {
      $result = mainlab_trait_listing();
    }
    else {
        $result = mainlab_trait_listing($form_state['values']['keyword']);
        $result .= '<script>jQuery("#mainlab_trait_listing-id").focus();</script>';
    }
    $form['result'] = array(
        '#markup' => $result,
        '#prefix' => '<div id="mainlab_trait_listing-result-field">',
        '#suffix' => '</div>'
    );
    return $form;
}

function mainlab_trait_listing_ajax_result ($form, &$form_state) {
    return $form['result'];
}

function mainlab_trait_listing ($keyword = NULL) {  
  $traits = mainlab_tripal_get_all_trait($keyword);
  $output = '';
  if (mainlab_tripal_count($traits) > 0) {
      $trait_def = variable_get('mainlab_tripal_show_trait_definition', 0);
      $output = '<table>';
      $output .= '<tr>
                        <th>Category</th>
                        <th><div  class="mainlab_tripal_hidden_header" style="display:none;">Abbreviation</div></th>
                        <th><div  class="mainlab_tripal_hidden_header" style="display:none;">Trait Name</div></th>
                        <th><div  class="mainlab_tripal_hidden_header" style="display:none;">Alias</div></th>';
      if ($trait_def) {
        $output .= '<th><div  class="mainlab_tripal_hidden_header" style="display:none;">Definition</div></th>';
      }
      $output .=   '<th nowrap>Count</th>
                      </tr>';
      $color = "#FFF'";
      $js = "
        var cat = jQuery(this).get(0).id;
        var cat_html = jQuery(this).html();
        var sign = cat_html.substring(0, 1);
        var text = cat_html.substring(2);
        if (sign == \"+\") {
          jQuery(this).html(\"- \" + text);
          jQuery(\".\" + cat).css(\"padding-bottom\", \"4px\");
          jQuery(\".\" + cat).show();
        }
        else {
          jQuery(this).html(\"+ \" + text);
          jQuery(\".\" + cat).css(\"padding-bottom\", \"0px\");
          jQuery(\".\" + cat).hide();
        }
        var all = document.getElementsByClassName(\"mainlab_tripal_hidden_element\");
        var i;
        var hide = 1;
        for (i = 0; i < all.length; i++) {
          var disp = all[i].style.display;
          if (disp != \"none\") {
            jQuery(\".mainlab_tripal_hidden_header\").show();
            hide = 0;
            break;
          }          
        }
        if (hide == 1) {
          jQuery(\".mainlab_tripal_hidden_header\").hide();
        }
        return false;
      ";
      $index = 0;
      foreach ($traits AS $cat => $trait) {
          $num_traits = mainlab_tripal_count($trait);
          if ($num_traits > 0) {
              $counter = 0;
              foreach ($trait AS $t) {
                $output .= "<tr style='background-color:$color'>";
                $padding = 'padding:0px 10px';
                if ($counter == 0) {
                  $output .= 
                    "<td rowspan='$num_traits' style='vertical-align:top;padding:4px 10px 4px;min-width:300px;'>
                       <a id='mainlab_tripal_hidden_element-$index' href='#' onClick='$js'>+ $cat</a>
                     </td>";
                  $padding = 'padding:4px 10px 0px';
                }
                $output .= 
                  "<td style='$padding;'>
                     <div class='mainlab_tripal_hidden_element mainlab_tripal_hidden_element-$index' style='display:none;'>" . $t['abbreviation'] . '</div>
                   </td>';
                $trait_name = $t['trait'];
                if ($keyword) {
                    $position = strpos(strtolower($t['trait']), strtolower($keyword));
                    if ($position !== FALSE) {
                        $start = substr($t['trait'], 0, $position);
                        $mid = substr($t['trait'], $position, strlen($keyword));
                        $end = substr($t['trait'],$position + strlen($keyword));
                        $trait_name = $start . "<b>$mid</b>" . $end;
                    }                    
                }
                $output .= 
                  "<td style='$padding;'>
                     <div class='mainlab_tripal_hidden_element mainlab_tripal_hidden_element-$index' style='display:none;min-width:260px !important'><a href='/trait/" . $t['cvterm_id'] . "'>" . $trait_name . '</a></div>
                   </td>';
                $output .=
                "<td style='$padding;'>
                     <div class='mainlab_tripal_hidden_element mainlab_tripal_hidden_element-$index' style='display:none;min-width:260px !important'>" . $t['synonym'] . '</div>
                   </td>';
                if ($trait_def) {
                  $output .=
                    "<td style='$padding;'>
                       <div class='mainlab_tripal_hidden_element mainlab_tripal_hidden_element-$index' style='display:none;'>" . $t['definition'] . '</div>
                     </td>';
                }
                if ($counter == 0) {
                  $output .= "<td rowspan='$num_traits'  style='vertical-align:top;padding:4px 10px 0px;background-color:$color'>" . $num_traits . "</td>";
                }
                $output .= '</tr>';
                $counter ++;
              }
              $color = $color == '#EEE' ? '#FFF' : '#EEE';
          }
          $index ++;
      }
      $output .= '</table>';
  }
  else {
      $output = '<div style="clear:both">No trait matches the keyword.</div>';
  }
  return $output;
}

function mainlab_tripal_get_all_trait($keyword = NULL) {
  $sql = "
   SELECT
      C.cvterm_id,
      CAT.name AS category,
      C.name AS trait,
      Alias.synonym,
      C.definition,
      A.value AS abbreviation,
      C.definition
    FROM cvterm C
    INNER JOIN cv ON C.cv_id = cv.cv_id
    LEFT JOIN (
      SELECT cvterm_id, value
      FROM cvtermprop
      WHERE type_id = (
        SELECT cvterm_id
        FROM cvterm C
        INNER JOIN cv on CV.cv_id = C.cv_id
        WHERE C.name = 'abbreviation' AND cv.name = 'MAIN')
    ) A on A.cvterm_id = C.cvterm_id
    LEFT JOIN (
      SELECT C1.name, C1.cvterm_id, CR.subject_id
      FROM cvterm C1
      INNER JOIN cvterm_relationship CR on CR.object_id = C1.cvterm_id
      INNER JOIN cvterm C2 on C2.cvterm_id = CR.type_id
      INNER JOIN cv CV2 on CV2.cv_id = C2.cv_id
      INNER JOIN cv CV3 on CV3.cv_id = C1.cv_id
      WHERE CV2.name = 'relationship' AND C2.name = 'is_a'
		AND CV3.name ~ 'trait_class_PTO'
    ) CAT on CAT.subject_id = C.cvterm_id
    LEFT JOIN (
      SELECT cvterm_id, string_agg(DISTINCT synonym, ' / ') AS synonym FROM cvtermsynonym WHERE trim(synonym) != '' GROUP BY cvterm_id
    ) ALIAS ON ALIAS.cvterm_id = C.cvterm_id
    WHERE cv.name LIKE '%_trait_ontology'
    AND CAT.name IS NOT NULL
    AND A.value IS NOT NULL ";
  if ($keyword) {
      $sql .= "AND lower(C.name) LIKE lower(:keyword) ";
  }
  $sql .= "ORDER BY CAT.name, A.value";
  $result = NULL;
  if ($keyword) {
    $result = chado_search_query($sql, array(':keyword' => '%' . $keyword . '%'));
  }
  else {
    $result = chado_search_query($sql);
  }
  $traits = array();
  while ($t = $result->fetchObject()) {
      if (!key_exists($t->category, $traits)) {
          $traits[$t->category] = array();
      }
      $traits[$t->category][] =  array(
          'abbreviation' => $t->abbreviation,
          'trait' => $t->trait,
          'synonym' => $t->synonym,
          'cvterm_id' => $t->cvterm_id,
          'definition' => $t->definition,
      );  
  } 
  return $traits;
}
