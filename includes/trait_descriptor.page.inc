<?php
/*
 * Trait Descriptor page
*/
function mainlab_trait_descriptor_page ($cvterm_id = NULL) {
    if (!is_numeric($cvterm_id)) {
        return '<h1>Page not found</h1>';
    }
  $obj = mainlab_tripal_get_trait_descriptor($cvterm_id);
  if (!$cvterm_id || (!property_exists($obj, 'cv'))) {
    return "<h1>Page not found</h1><p>The requested Trait Descriptor could not be found.</p>";
  }
  $num_alias = mainlab_tripal_count($obj->alias);
  $num_dataset =mainlab_tripal_count($obj->projects);
  $num_stock = $obj->num_stocks;
  $num_imgs = mainlab_tripal_count($obj->images);
  // prepare stocks
  $sql_stocks = "
  SELECT Sample.stock_id AS sample_id, Sample.uniquename AS sample_name, P.value, S.stock_id, S.uniquename AS stock_uniquename, O.organism_id, species
  FROM {phenotype} P
  INNER JOIN {nd_experiment_phenotype} NEP ON P.phenotype_id = NEP.phenotype_id
  INNER JOIN {nd_experiment_stock} NES ON NEP.nd_experiment_id = NES.nd_experiment_id
  INNER JOIN {stock} Sample ON Sample.stock_id = NES.stock_id
  INNER JOIN {stock_relationship} SR ON SR.subject_id = Sample.stock_id AND SR.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'sample_of' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
  INNER JOIN {stock} S ON S.stock_id = SR.object_id
  INNER JOIN {organism} O ON O.organism_id = S.organism_id
  WHERE P.attr_id =  :cvterm_id
  ";
  $num_per_page = 25;
  $pager = mainlab_tripal_get_pager($num_stock, $num_per_page, 'stocks');
  $stocks = mainlab_tripal_pager_query($sql_stocks, array(':cvterm_id' => $cvterm_id), $num_per_page, $pager['page'] - 1, TRUE);
  $all_stocks = mainlab_tripal_pager_query($sql_stocks, array(':cvterm_id' => $cvterm_id), 'ALL', 0, TRUE);
      
  // Generate Base content
  $rows = array();
  $rows[] = array( array('data' => 'Trait Descriptor', 'header' => TRUE, 'width' => '20%'), $obj->descriptor);
  if ($obj->trait) {
    $rows[] = array( array('data' => 'Of Trait', 'header' => TRUE, 'width' => '20%'), t('<a href="@url">' . $obj->trait . '</a>', array('@url' => '/trait/' . $obj->trait_id)));
  }
  $rows[] = array( array('data' => 'Definition', 'header' => TRUE, 'width' => '20%'), $obj->definition);
  if ($num_alias > 0) {
    $rows[] = array( array('data' => 'Alias', 'header' => TRUE, 'width' => '20%'), '[<a href="?pane=alias">view all ' . $num_alias . '</a>]');
  }
  $rows[] = array( array('data' => 'Format', 'header' => TRUE, 'width' => '20%'), $obj->format);
  if ($obj->category) {
    $rows[] = array( array('data' => 'Trait Category', 'header' => TRUE, 'width' => '20%'), $obj->category);
  }
  if ($num_dataset > 0) {
    $rows[] = array( array('data' => 'Datasets', 'header' => TRUE, 'width' => '20%'), '[<a href="?pane=dataset">view all ' . $num_dataset . '</a>]');
  }
  if ($num_stock > 0) {    
    $rows[] = array( array('data' => 'Germplasm', 'header' => TRUE, 'width' => '20%'), '[<a href="?pane=stocks">view all ' . $num_stock . '</a>]');
  }
  $table = array(
    'rows' => $rows,
    'attributes' => array(
      'id' => 'mainlab_tripal_trait_descriptor-table-base',
      'class' => 'tripal-data-table'
    ),
    'header' => NULL,
    'caption' => NULL,
    'colgroups' => NULL,
    'sticky' => NULL,
    'empty' => NULL,
  );
  $html_base = theme_table($table);
  $base = '<div id="base-tripal-data-pane" class="tripal-data-pane"> <div class="base-tripal-data-pane-title tripal-data-pane-title">Descriptor Overview</div><div class="tripal-data-block-desc"></div>' . $html_base . '</div>';
  
  // Generate Alias content
  $rows = array();
  foreach($obj->alias AS $a) {
    $rows[] = array($a);
  }
  $table = array(
    'rows' => $rows,
    'attributes' => array(
      'id' => 'mainlab_tripal_trait_descriptor-table-alias',
      'class' => 'tripal-data-table'
    ),
    'header' => NULL,
    'caption' => NULL,
    'colgroups' => NULL,
    'sticky' => NULL,
    'empty' => NULL,
  );
  $html_alias = theme_table($table);
  $alias = '<div id="alias-tripal-data-pane" class="tripal-data-pane"> <div class="descriptors-tripal-data-pane-title tripal-data-pane-title">Alias</div><div class="tripal-data-block-desc"></div>' . $html_alias  . '</div>';
  
  // Generate Dataset content
  $rows = array();
  foreach($obj->projects AS $j) {
    $rows[] = array(t("<a href='@nid'>" . $j['name'] . "</a>", array('@nid' => mainlab_tripal_link_record('project', $j['project_id']))), $j['type']);
  }
  $table = array(
    'rows' => $rows,
    'attributes' => array(
      'id' => 'mainlab_tripal_trait_descriptor-table-dataset',
      'class' => 'tripal-data-table'
    ),
    'header' => ['Dataset', 'Type'],
    'caption' => NULL,
    'colgroups' => NULL,
    'sticky' => NULL,
    'empty' => NULL,
  );
  $html_dataset = theme_table($table);
  $dataset = '<div id="dataset-tripal-data-pane" class="tripal-data-pane"> <div class="dataset-tripal-data-pane-title tripal-data-pane-title">Datasets</div><div class="tripal-data-block-desc"></div>' . $html_dataset  . '</div>';
  
  
  // Generate Stocks content
  $rows = array();
  $hearers = array('Sample name', 'Sepecies', 'Germplasm', $obj->descriptor);
  foreach($stocks AS $s) {
    $rows[] = 
      array(
        t("<a href='@nid'>" . $s->sample_name . "</a>", array('@nid' => mainlab_tripal_link_record('stock', $s->sample_id))),
        t("<a href='@nid'>" . $s->species . "</a>", array('@nid' => mainlab_tripal_link_record('organism', $s->organism_id))),
        t("<a href='@nid'>" . $s->stock_uniquename . "</a>", array('@nid' => mainlab_tripal_link_record('stock', $s->stock_id))),
        $s->value);
  }
  $table = array(
    'rows' => $rows,
    'attributes' => array(
      'id' => 'mainlab_tripal_trait_descriptor-table-stocks',
      'class' => 'tripal-data-table'
    ),
    'header' => $hearers,
    'caption' => NULL,
    'colgroups' => NULL,
    'sticky' => NULL,
    'empty' => NULL,
  );
  $html_stocks = theme_table($table);
  $html_stocks .= $pager['pager'];
  // Preparing download
  $dir = 'sites/default/files/tripal/mainlab_tripal/download';
  if (!file_exists($dir)) {
      mkdir ($dir, 0777);
  }
  $download = $dir . '/trait_descriptor_id_' . $cvterm_id . '.csv';
  $handle = fopen($download, "w");
  fwrite($handle, "Trait Descriptor: " . $obj->descriptor. "\n");
  fwrite($handle, '"#","Sample Name","Species","Germplasm","Descriptor"' . "\n");
  $counter = 0;
  foreach ($all_stocks AS $stk) {
      fwrite($handle, '"' . ($counter + 1) . '","' . $stk->sample_name . '","' . $stk->species . '","' . $stk->stock_uniquename . '","' . $stk->value . '"' . "\n");
      $counter ++;
  }
  fclose($handle);
  $stocks = '<div id="stocks-tripal-data-pane" class="tripal-data-pane"> <div class="stocks-tripal-data-pane-title tripal-data-pane-title">Germplasm</div><div class="tripal-data-block-desc"></div><div style="float: right;margin-bottom:15px;">Download <a href="/' . $download . '">Table</a></div>' . $html_stocks  . '</div>';

  // Generate Image content
  if ($num_imgs > 0) {
      $images = $obj->images;
      
      $html_images = '<div id="images-tripal-data-pane" class="tripal-data-pane">';
      if (mainlab_tripal_count($images) > 0) {
          $rows = array();
          foreach ($images AS $img) {
              $link = mainlab_tripal_link_record('eimage', $img->eimage_id);
              if ($link) {
                  $rows [] = array ("<a href='$link' style='white-space:nowrap'>". $img->image_uri . '</a>', $img->legend);
              }
              else {
                  $rows [] = array ($img->image_uri, $img->legend);
              }
          }
          $header = array ('File Name', 'Legend');
          $table = array(
              'header' => $header,
              'rows' => $rows,
              'attributes' => array(
                  'id' => 'tripal_trait_descriptor-table-image',
              ),
              'sticky' => FALSE,
              'caption' => '',
              'colgroups' => array(),
              'empty' => '',
          );
          $html_images .= theme_table($table);
      }
      $html_images .= '</div>';
  }
  
  // Apply the sidebar
  $core_path = drupal_get_path('module', 'tripal_core');
  $markup = '
             <div class="tripal_toc_list_item">
               <a id="base" class="tripal_toc_list_item_link" href="?pane=base">Descriptor Overview</a>
             </div>';
  if ($num_alias > 0) {
    $markup .= '
             <div class="tripal_toc_list_item">
               <a id="alias" class="tripal_toc_list_item_link" href="?pane=alias">Alias</a>
             </div>';
  }
  if ($num_dataset > 0) {
    $markup .= '
             <div class="tripal_toc_list_item">
               <a id="dataset" class="tripal_toc_list_item_link" href="?pane=dataset">Datasets</a>
             </div>';
  }
  if ($num_stock > 0) {
    $markup .= '
             <div class="tripal_toc_list_item">
               <a id="stocks" class="tripal_toc_list_item_link" href="?pane=stocks">Germplasm</a>
             </div>';
  }
  if ($num_imgs > 0) {
      $markup .= '
             <div class="tripal_toc_list_item">
               <a id="images" class="tripal_toc_list_item_link" href="?pane=images">Image</a>
             </div>';
  }
  $node = new stdClass();
  $node->type = 'mainlab_tripal_trait';
  $variables = array(
    'teaser' => NULL,
    'node' => $node,
    'content' => array(
      'mainlab_tripal_trait_descriptor_base' => array(
        '#markup' => $base,
        '#tripal_toc_id' => 'base',
        '#weight' => -100,
        '#hide' => 0
      ),
      'mainlab_tripal_trait_descriptor_descriptors' => array(
        '#markup' => $alias,
        '#tripal_toc_id' => 'descriptors',
        '#weight' => -80,
        '#hide' => 0
      ),
      'mainlab_tripal_trait_descriptor_dataset' => array(
        '#markup' => $dataset,
        '#tripal_toc_id' => 'dataset',
        '#weight' => -60,
        '#hide' => 0
      ),
      'mainlab_tripal_trait_descriptor_stocks' => array(
        '#markup' => $stocks,
        '#tripal_toc_id' => 'stocks',
        '#weight' => -40,
        '#hide' => 0
      ),
      'mainlab_tripal_trait_images' => array(
        '#markup' => $html_images,
        '#tripal_toc_id' => 'images',
        '#weight' => -20,
        '#hide' => 0
      ),
      'tripal_toc' => array(
        '#markup' =>
        '<div id="chado_analysis-tripal-toc-pane" class="tripal-toc-pane">' . $markup . '</div>'
      )
    )
  );
  
  $theme_core = theme_render_template("$core_path/theme/templates/node--chado-generic.tpl.php", $variables);
  
  return $theme_core;
}

function mainlab_tripal_get_trait_descriptor($cvterm_id) {
  if (!$cvterm_id) {
    return NULL;
  }
  $sql = "
    SELECT 
      CV.name AS cv,
      CAT.name AS category,
      TRAIT.cvterm_id AS trait_id, 
      TRAIT.name AS trait, 
      TRAIT.cv AS trait_cv,
      C.name AS descriptor, 
      F.value AS format, 
      C.definition
    FROM {cvterm} C
    INNER JOIN {cv} CV on CV.cv_id = C.cv_id
    INNER JOIN chado.cvprop CVP on CVP.cv_id = CV.cv_id
    INNER JOIN chado.cvterm S on S.cvterm_id = CVP.type_id AND S.name = 'descriptor_search'
    INNER JOIN chado.cv S_CV on S_CV.cv_id = S.cv_id AND S_CV.name = 'MAIN'
    LEFT JOIN (
      SELECT cvterm_id, value 
      FROM {cvtermprop}
      WHERE type_id = (
        SELECT cvterm_id FROM {cvterm} C
        INNER JOIN {cv} on CV.cv_id = C.cv_id
        WHERE C.name = 'format' AND cv.name = 'MAIN'
      )
    ) F on F.cvterm_id = C.cvterm_id
    LEFT JOIN (
      SELECT (SELECT name FROM {cv} WHERE cv_id = C1.cv_id) AS cv, C1.name, C1.cvterm_id, CR.subject_id
        FROM {cvterm} C1
        INNER JOIN {cvterm_relationship} CR on CR.object_id = C1.cvterm_id
        INNER JOIN {cvterm} C2 on C2.cvterm_id = CR.type_id
        INNER JOIN {cv} CV2 on CV2.cv_id = C2.cv_id
        WHERE CV2.name = 'MAIN' AND C2.name = 'belongs_to'
    ) TRAIT on TRAIT.subject_id = C.cvterm_id
    LEFT JOIN (
      SELECT C1.name, C1.cvterm_id, CR.subject_id
      FROM {cvterm} C1
      INNER JOIN {cvterm_relationship} CR on CR.object_id = C1.cvterm_id
      INNER JOIN {cvterm} C2 on C2.cvterm_id = CR.type_id
      INNER JOIN {cv} CV2 on CV2.cv_id = C2.cv_id
      WHERE CV2.name = 'relationship' AND C2.name = 'is_a'
    ) CAT on CAT.subject_id = TRAIT.cvterm_id
    WHERE C.cvterm_id = :cvterm_id";
  $trait_descriptor = chado_query($sql, array(':cvterm_id' => $cvterm_id))->fetchObject();
  
  $sql_alias = "
  SELECT synonym AS alais
  FROM {cvtermsynonym} CS
  WHERE cvterm_id = :cvterm_id
  ";
  $alias = array();
  $result = chado_query($sql_alias,  array(':cvterm_id' => $cvterm_id));
  while ($d = $result->fetchObject()) {
    $alias[] = $d->alais;
  }
  if (!$trait_descriptor) {
    return new stdClass();
  }
  $trait_descriptor->alias = $alias;
  
  $sql_stocks = "
  SELECT count(*)
  FROM {phenotype} P
  INNER JOIN {nd_experiment_phenotype} NEP ON P.phenotype_id = NEP.phenotype_id
  INNER JOIN {nd_experiment_stock} NES ON NEP.nd_experiment_id = NES.nd_experiment_id
  INNER JOIN {stock} Sample ON Sample.stock_id = NES.stock_id
  INNER JOIN {stock_relationship} SR ON SR.subject_id = Sample.stock_id AND SR.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'sample_of' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
  INNER JOIN {stock} S ON S.stock_id = SR.object_id
  WHERE P.attr_id =  :cvterm_id
  ";
  $num_stocks = chado_query($sql_stocks,  array(':cvterm_id' => $cvterm_id))->fetchField();
  $trait_descriptor->num_stocks = $num_stocks;
  /*$result = chado_query($sql_stocks,  array(':cvterm_id' => $cvterm_id));
   while ($s = $result->fetchObject()) {
    $stocks[$s->sample_id] = array('sample' => $s->sample_name, 'value' => $s->value, 'stock_id' => $s->stock_id, 'stock_uniquename' => $s->stock_uniquename);
  }
  $trait_descriptor->stocks = $stocks;
  */
  
   $sql_proj = "
  SELECT DISTINCT J.project_id, J.name, JP.value AS type
  FROM {phenotype} P
  INNER JOIN {nd_experiment_phenotype} NEP ON P.phenotype_id = NEP.phenotype_id
  INNER JOIN {nd_experiment_project} NEJ ON NEP.nd_experiment_id = NEJ.nd_experiment_id
  INNER JOIN {project} J ON J.project_id = NEJ.project_id
INNER JOIN chado.projectprop JP ON JP.project_id = J.project_id AND JP.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'project_type' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
  WHERE P.attr_id =  :cvterm_id
  ";
  $projects = array();
  $result = chado_query($sql_proj,  array(':cvterm_id' => $cvterm_id));
  while ($j = $result->fetchObject()) {
    $projects[] = [
      'project_id' => $j->project_id,
      'name' => $j->name,
      'type' => $j->type
    ];
  }
  $trait_descriptor->projects = $projects;
  
  // Get images
  $results = db_query(
      "SELECT I.eimage_id, eimage_type, image_uri, value as legend
       FROM {chado.cvterm_image} CI
       INNER JOIN {chado.eimage} I ON CI.eimage_id = I.eimage_id
       INNER JOIN {chado.eimageprop} IP ON I.eimage_id = IP.eimage_id
       WHERE IP.type_id = (SELECT cvterm_id
                                            FROM {chado.cvterm}
                                            WHERE name = 'legend'
                                            AND cv_id = (SELECT cv_id FROM {chado.cv} WHERE name = 'MAIN')
                                          )
      AND cvterm_id = :cvterm_id",
      array(':cvterm_id' => $cvterm_id));
  $images = array();
  while ($obj = $results->fetchObject()) {
      $images [] = $obj;
  }
  $trait_descriptor->images = $images;
  
  return $trait_descriptor;
}
