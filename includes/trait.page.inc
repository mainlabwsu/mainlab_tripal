<?php
/*
 * Trait page
*/
function mainlab_trait_page ($cvterm_id = NULL) {

  $obj = mainlab_tripal_get_trait($cvterm_id);
  if (!$cvterm_id || !preg_match('/.+_trait_ontology$/', $obj->cv)) {
    return "<h1>Page not found</h1><p>The requested Trait could not be found.</p>";
  }
  $num_descriptors = mainlab_tripal_count($obj->descriptors);
  $num_qtls = mainlab_tripal_count($obj->qtls);
  $num_imgs = mainlab_tripal_count($obj->images);
  $num_projects = mainlab_tripal_count($obj->projects);
  $num_pubs = mainlab_tripal_count($obj->pubs);
  
  // Generate Base content
  $rows = array();
  $rows[] = array( array('data' => 'Trait', 'header' => TRUE, 'width' => '20%'), $obj->trait);
  $rows[] = array( array('data' => 'Trait Category', 'header' => TRUE, 'width' => '20%'), $obj->category);
  $rows[] = array( array('data' => 'Abbreviation', 'header' => TRUE, 'width' => '20%'), $obj->abbreviation);
  $rows[] = array( array('data' => 'Definition', 'header' => TRUE, 'width' => '20%'), $obj->definition);
  if ($num_descriptors > 0) {
    $rows[] = array( array('data' => 'Descriptors', 'header' => TRUE, 'width' => '20%'), '[<a href="?pane=descriptors">view all ' . $num_descriptors . '</a>]');
  }
  if ($num_qtls > 0) {
    $rows[] = array( array('data' => 'QTLs', 'header' => TRUE, 'width' => '20%'), '[<a href="?pane=qtls">view all ' . $num_qtls . '</a>]');
  }
  $table = array(
    'rows' => $rows,
    'attributes' => array(
      'id' => 'mainlab_tripal_trait-table-base',
      'class' => 'tripal-data-table'
    ),
    'header' => NULL,
    'caption' => NULL,
    'colgroups' => NULL,
    'sticky' => NULL,
    'empty' => NULL,
  );
  $html_base = theme_table($table);
  $base = '<div id="base-tripal-data-pane" class="tripal-data-pane"> <div class="base-tripal-data-pane-title tripal-data-pane-title">Trait Overview</div><div class="tripal-data-block-desc"></div>' . $html_base . '</div>';

  // Generate Descriptors content
  if ($num_descriptors > 0) {
    $rows = array();
    $header = array('Group', 'Descriptor');
    foreach($obj->descriptors AS $id => $d) {
      $rows[] = array($d['group'], t("<a href='@url'>" . $d['descriptor'] . "</a>", array('@url' => "/trait_descriptor/$id")));
    }
    $table = array(
      'rows' => $rows,
      'attributes' => array(
        'id' => 'mainlab_tripal_trait-table-descriptors',
        'class' => 'tripal-data-table'
      ),
        'header' => $header,
      'caption' => NULL,
      'colgroups' => NULL,
      'sticky' => NULL,
      'empty' => NULL,
    );
    $html_descriptors = theme_table($table);
    $descriptors = '<div id="descriptors-tripal-data-pane" class="tripal-data-pane"> <div class="descriptors-tripal-data-pane-title tripal-data-pane-title">Descriptors</div><div class="tripal-data-block-desc"></div>' . $html_descriptors  . '</div>';
  }

  // Generate QTLs content
  if ($num_qtls > 0) {
    $dir = 'sites/default/files/tripal/mainlab_tripal/download';
    if (!file_exists($dir)) {
      mkdir ($dir, 0777);
    }
    $download = $dir . '/trait_qtl_data_cvterm_id_' . $cvterm_id . '.csv';
    $handle = fopen($download, "w");
    fwrite($handle, "QTL/MTL Data for Trait '" . $obj->trait . "'\n");
    fwrite($handle, '"QTL/MTL","Linkage Group","Peak","Start","Stop","Dataset"' . "\n");
    $rows = array();
    $sql_qtl = "SELECT (SELECT genus || ' ' || species FROM chado.organism WHERE organism_id = F.organism_id) AS species, (SELECT name FROM chado.cvterm WHERE cvterm_id = F.type_id) AS type FROM chado.feature F WHERE feature_id = :feature_id";
    
    foreach($obj->qtls AS $feature_id => $q) {
      //$qtl_details = db_query($sql_qtl, array(':feature_id' => $feature_id))->fetchObject();
      //$type = $qtl_details->type == 'heritable_phenotypic_marker' ? 'MTL': $qtl_details->type;
      $jlink = mainlab_tripal_link_record('project', $q['project_id']);
      $qtl_pj = $q['project'] ? $q['project'] : 'na';
      $lg = $q['std_lg'] ? $q['std_lg'] : ($q['lg'] ? $q['lg'] : '-'); // Use std_lg when available
      $mapviewer_link = $q['lg'] ? '<a href="/mapviewer/'. $q['featuremap_id'] . '/' . $lg . '/'. $feature_id . '">View</a>' : '-';
      if ($q['type'] == 'GWAS') {
        $mapviewer_link = '<a href="/mapviewer_gwas/'. $feature_id . '">View</a>';
      }
      $rows[] = array(
        t("<a href='@nid'>" . $q['qtl'] . "</a>", array('@nid' => mainlab_tripal_link_record('feature', $feature_id))),
        //$qtl_details->species,
        //$type,
        $lg,
        $q['peak'] ? $q['peak'] : '-',
        $q['start'] ? $q['start'] : '-',
        $q['stop'] ? $q['stop'] : '-',
        $jlink ? '<a href="' . $jlink . '">' . $qtl_pj . '</a>' : $qtl_pj,
        $mapviewer_link
      );
      fwrite($handle, $q['qtl'] . "," . $q['lg'] . "," . $q['peak'] . "," . $q['start'] . "," . $q['stop'] . "," . $q['project'] . "\n");
    }
    fclose($handle);
    $table = array(
      'rows' => $rows,
      'attributes' => array(
        'id' => 'mainlab_tripal_trait-table-descriptors',
        'class' => 'tripal-data-table'
      ),
      'header' => array(
        'QTL/MTL',
        //'Species',
        //'Type',
        'Linkage Group',
        'Peak',
        'Start',
        'Stop',
        'Dataset',
        'MapViewer'),
      'caption' => NULL,
      'colgroups' => NULL,
      'sticky' => NULL,
      'empty' => NULL,
    );
    $html_qtls = theme_table($table);
    $html_download =
      "<div style=\"float: right;margin-bottom:15px\">Download <a href=\"/" . $download . "\">Table</a></div>";
    $qtls = '<div id="qtls-tripal-data-pane" class="tripal-data-pane"> <div class="qtls-tripal-data-pane-title tripal-data-pane-title">QTLs</div><div class="tripal-data-block-desc"></div>' . $html_download . $html_qtls  . '</div>';
  }

  // Generate Image content
  if ($num_imgs > 0) {
      $images = $obj->images;
      $icon_path = '';
      $img_path = '';
      if (mainlab_tripal_get_site() == 'cottongen') {
          $icon_path ='sites/default/files/bulk_data/www.cottongen.org/cotton_photo/z_trait/icon/icon-';
          $img_path = 'sites/default/files/bulk_data/www.cottongen.org/cotton_photo/z_trait/image/';
          $alt_ico_path = 'sites/default/files/bulk_data/www.cottongen.org/cotton_photo/non-ncgc/icon/icon-';
          $alt_img_path = 'sites/default/files/bulk_data/www.cottongen.org/cotton_photo/non-ncgc/image/';
      }
      $html_images = '<div id="images-tripal-data-pane" class="tripal-data-pane">';
      if (mainlab_tripal_count($images) > 0) {
          $rows = array();
          foreach ($images AS $img) {
              $link = mainlab_tripal_link_record('eimage', $img->eimage_id);
              if ($link) {
                  $rows [] = array ("<a href='$link' style='white-space:nowrap'>". $img->image_uri . '</a>', $img->legend);
              }
              else {
                  $rows [] = array ($img->image_uri, $img->legend);
              }
          }
          $header = array ('File Name', 'Legend');
          $table = array(
              'header' => $header,
              'rows' => $rows,
              'attributes' => array(
                  'id' => 'tripal_trait-table-image',
              ),
              'sticky' => FALSE,
              'caption' => '',
              'colgroups' => array(),
              'empty' => '',
          );
          $html_images .= theme_table($table);
      }
      $html_images .= '</div>';
  }
  
  // Generate Projects content
  if ($num_projects > 0) {
    $html_projects = '<div id="projects-tripal-data-pane" class="tripal-data-pane">';
    $rows = array();
    foreach ($obj->projects AS $proj) {
      $link = mainlab_tripal_link_record('project', $proj['project_id']);
      if ($link) {
        $rows [] = array ("<a href='$link' style='white-space:nowrap'>". $proj['name'] . '</a>', $proj['type']);
      }
      else {
        $rows [] = array ($proj['name'], $proj['type']);
      }
    }
    $header = array ('Dataset', 'Type');
    $table = array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
        'id' => 'tripal_trait-table-projects',
      ),
      'sticky' => FALSE,
      'caption' => '',
      'colgroups' => array(),
      'empty' => '',
    );
    $html_projects .= theme_table($table);
    $html_projects .= '</div>';
  }

  // Generate Pubs content
  if ($num_pubs > 0) {
    $html_pubs = '<div id="pubs-tripal-data-pane" class="tripal-data-pane">';
    $rows = array();
    foreach ($obj->pubs AS $pub) {
      $link = mainlab_tripal_link_record('pub', $pub['pub_id']);
      if ($link) {
        $rows [] = array ($pub['pyear'], "<a href='$link''>". $pub['uniquename'] . '</a>');
      }
      else {
        $rows [] = array ($pub['pyear'], $pub['uniquename']);
      }
    }
    $header = array ('Year', 'Publication');
    $table = array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
        'id' => 'tripal_trait-table-pubs',
      ),
      'sticky' => FALSE,
      'caption' => '',
      'colgroups' => array(),
      'empty' => '',
    );
    $html_pubs .= theme_table($table);
    $html_pubs .= '</div>';
  }
  
  // Apply the sidebar
  $core_path = drupal_get_path('module', 'tripal_core');
  $markup = '
             <div class="tripal_toc_list_item">
               <a id="base" class="tripal_toc_list_item_link" href="?pane=base">Trait Overview</a>
             </div>';
  if ($num_descriptors > 0) {
    $markup .= '
             <div class="tripal_toc_list_item">
               <a id="descriptors" class="tripal_toc_list_item_link" href="?pane=descriptors">Descriptors</a>
             </div>';
  }
  if ($num_qtls > 0) {
    $markup .= '
             <div class="tripal_toc_list_item">
               <a id="qtls" class="tripal_toc_list_item_link" href="?pane=qtls">QTLs/MTLs</a>
             </div>';
  }
  if ($num_imgs > 0) {
      $markup .= '
             <div class="tripal_toc_list_item">
               <a id="images" class="tripal_toc_list_item_link" href="?pane=images">Image</a>
             </div>';
  }
  if ($num_projects > 0) {
    $markup .= '
             <div class="tripal_toc_list_item">
               <a id="projects" class="tripal_toc_list_item_link" href="?pane=projects">Dataset</a>
             </div>';
  }
  if ($num_pubs > 0) {
    $markup .= '
             <div class="tripal_toc_list_item">
               <a id="pubs" class="tripal_toc_list_item_link" href="?pane=pubs">Publications</a>
             </div>';
  }
  $node = new stdClass();
  $node->type = 'mainlab_tripal_trait';
  $variables = array(
    'teaser' => NULL,
    'node' => $node,
    'content' => array(
      'mainlab_tripal_trait_base' => array(
        '#markup' => $base,
        '#tripal_toc_id' => 'base',
        '#weight' => -100,
        '#hide' => 0
      ),
      'mainlab_tripal_trait_descriptors' => array(
        '#markup' => $descriptors,
        '#tripal_toc_id' => 'descriptors',
        '#weight' => -80,
        '#hide' => 0
      ),
      'mainlab_tripal_trait_qtls' => array(
        '#markup' => $qtls,
        '#tripal_toc_id' => 'qtls',
        '#weight' => -60,
        '#hide' => 0
      ),
      'mainlab_tripal_trait_images' => array(
        '#markup' => $html_images,
        '#tripal_toc_id' => 'images',
        '#weight' => -40,
        '#hide' => 0
      ),
      'mainlab_tripal_trait_projects' => array(
        '#markup' => $html_projects,
        '#tripal_toc_id' => 'projects',
        '#weight' => -20,
        '#hide' => 0
      ),
      'mainlab_tripal_trait_pubs' => array(
        '#markup' => $html_pubs,
        '#tripal_toc_id' => 'pubs',
        '#weight' => 0,
        '#hide' => 0
      ),
      'tripal_toc' => array(
        '#markup' =>
          '<div id="chado_analysis-tripal-toc-pane" class="tripal-toc-pane">' . $markup . '</div>'
      )
    )
  );

  $theme_core = theme_render_template("$core_path/theme/templates/node--chado-generic.tpl.php", $variables);

  return $theme_core;
}

function mainlab_tripal_get_trait($cvterm_id) {
  if (!$cvterm_id) {
    return NULL;
  }
  $sql = "
    SELECT
      string_agg(DISTINCT CV.name, '; ') AS cv,
      string_agg(DISTINCT CAT.name, '; ') AS category,
      string_agg(DISTINCT C.name, '; ') AS trait,
      string_agg(DISTINCT A.value, '; ') AS abbreviation,
      string_agg(DISTINCT C.definition, '; ') AS definition
    FROM {cvterm} C
    INNER JOIN {cv} ON C.cv_id = cv.cv_id
    LEFT JOIN (
      SELECT cvterm_id, value
      FROM {cvtermprop}
      WHERE type_id = (
        SELECT cvterm_id
        FROM {cvterm} C
        INNER JOIN {cv} on CV.cv_id = C.cv_id
        WHERE C.name = 'abbreviation' AND cv.name = 'MAIN')
    ) A on A.cvterm_id = C.cvterm_id
    LEFT JOIN (
      SELECT C1.name, C1.cvterm_id, CR.subject_id
      FROM {cvterm} C1
      INNER JOIN {cvterm_relationship} CR on CR.object_id = C1.cvterm_id
      INNER JOIN {cvterm} C2 on C2.cvterm_id = CR.type_id
      INNER JOIN {cv} CV2 on CV2.cv_id = C2.cv_id
      WHERE CV2.name = 'relationship' AND C2.name = 'is_a'
    ) CAT on CAT.subject_id = C.cvterm_id
    WHERE C.cvterm_id = :cvterm_id
    GROUP BY C.cvterm_id";
  $trait = chado_query($sql, array(':cvterm_id' => $cvterm_id))->fetchObject();
  if (!$trait) {
    return NULL;
  }

  $sql_descriptors = "
  SELECT
    C1.cvterm_id,
    C1.name AS descriptor,
    CASE
      WHEN (SELECT value FROM {cvprop} WHERE cv_id = C1.cv_id AND type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'searchable' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))) IS NOT NULL
      THEN   (SELECT value FROM {cvprop} WHERE cv_id = C1.cv_id AND type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'searchable' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')))
      ELSE     (SELECT name FROM {cv} WHERE cv_id = C1.cv_id)
      END
    AS group
  FROM {cvterm} C1
  INNER JOIN {cvterm_relationship} CR on CR.subject_id = C1.cvterm_id
  INNER JOIN {cvterm} C2 on C2.cvterm_id = CR.type_id
  INNER JOIN {cv} CV2 on CV2.cv_id = C2.cv_id
  WHERE CV2.name = 'MAIN' AND C2.name = 'belongs_to'
  AND object_id = :cvterm_id
  ";
  $descriptors = array();
  $result = chado_query($sql_descriptors,  array(':cvterm_id' => $cvterm_id));
  while ($d = $result->fetchObject()) {
    $descriptors[$d->cvterm_id] = array(
      'group' =>  $d->group,
      'descriptor' =>  $d->descriptor,
    );
  }
  $trait->descriptors = $descriptors;

  $sql_qtls = "
  SELECT
    FM.featuremap_id,
    FM.name AS name,
    LG.name AS linkage_group,
    (SELECT value FROM featureprop WHERE feature_id = LG.feature_id AND type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'chr_name' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))) AS std_lg,
    START.value AS QTL_start,
    STOP.value AS QTL_stop,
    PEAK.value AS QTL_peak,
    QTL.feature_id,
    (SELECT name FROM chado.cvterm WHERE cvterm_id = QTL.type_id) AS type,
    QTL.uniquename AS qtl,
    J.project_id,
    J.name AS project
  FROM {feature_cvterm} FC
  INNER JOIN {feature} QTL on QTL.feature_id = FC.feature_id
  LEFT JOIN {featurepos} FP ON QTL.feature_id = FP.feature_id
  LEFT JOIN {featuremap} FM ON FP.featuremap_id = FM.featuremap_id
  LEFT JOIN {feature} LG ON FP.map_feature_id = LG.feature_id
  LEFT JOIN {feature_project} FJ ON FJ.feature_id = FC.feature_id
  LEFT JOIN {project} J ON J.project_id = FJ.project_id
  LEFT JOIN (
  SELECT featurepos_id, value
  FROM {featureposprop} FPP
  WHERE FPP.type_id =
  (SELECT cvterm_id
  FROM {cvterm}
  WHERE name = 'start' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
  ) START ON FP.featurepos_id = START.featurepos_id
  LEFT JOIN (
  SELECT featurepos_id, value
  FROM {featureposprop} FPP
  WHERE FPP.type_id =
  (SELECT cvterm_id
  FROM {cvterm}
  WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
  ) STOP ON FP.featurepos_id = STOP.featurepos_id
  LEFT JOIN (
  SELECT featurepos_id, value
  FROM {featureposprop} FPP
  WHERE FPP.type_id =
  (SELECT cvterm_id
  FROM {cvterm}
           WHERE name = 'qtl_peak' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
    ) PEAK ON FP.featurepos_id = PEAK.featurepos_id
  WHERE cvterm_id = :cvterm_id
  AND QTL.type_id IN (SELECT cvterm_id FROM cvterm WHERE name IN ('GWAS', 'QTL', 'heritable_phenotypic_marker'))
  ORDER BY QTL.type_id, QTL.uniquename
  ";
  $qtls = array();
  $result = chado_query($sql_qtls,  array(':cvterm_id' => $cvterm_id));
  while ($q = $result->fetchObject()) {
    $qtls[$q->feature_id] =
    array(
      'qtl' => $q->qtl,
      'type' => $q->type,
      'featuremap_id' => $q->featuremap_id,
      'map' => $q->name,
      'lg' => $q->linkage_group,
      'std_lg' => $q->std_lg,
      'start' => $q->qtl_start,
      'stop' => $q->qtl_stop,
      'peak' => $q->qtl_peak,
      'project_id' => $q->project_id,
      'project' => $q->project
    );
  }
  $trait->qtls = $qtls;
  // Get images
  $results = db_query(
      "SELECT I.eimage_id, eimage_type, image_uri, value as legend
       FROM {chado.cvterm_image} CI
       INNER JOIN {chado.eimage} I ON CI.eimage_id = I.eimage_id
       INNER JOIN {chado.eimageprop} IP ON I.eimage_id = IP.eimage_id
       WHERE IP.type_id = (SELECT cvterm_id
                                            FROM {chado.cvterm}
                                            WHERE name = 'legend'
                                            AND cv_id = (SELECT cv_id FROM {chado.cv} WHERE name = 'MAIN')
                                          )
      AND cvterm_id = :cvterm_id",
      array(':cvterm_id' => $cvterm_id));
  $images = array();
  while ($obj = $results->fetchObject()) {
      $images [] = $obj;
  }
  $trait->images = $images;
  
  // Get projects
  $sql_projects = "
    SELECT DISTINCT J.project_id, J.name, JP.value AS type
    FROM chado.project J
	    INNER JOIN chado.projectprop JP ON JP.project_id = J.project_id AND JP.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'project_type' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
      INNER JOIN chado.nd_experiment_project NEJ on NEJ.project_id = J.project_id
      INNER JOIN chado.nd_experiment_phenotype NEP on NEP.nd_experiment_id = NEJ.nd_experiment_id
      INNER JOIN chado.phenotype P on P.phenotype_id = NEP.phenotype_id
      INNER JOIN chado.cvterm D on D.cvterm_id = P.attr_id
      INNER JOIN chado.cvterm_relationship CR on CR.subject_id = D.cvterm_id
      INNER JOIN chado.cvterm TYPE on TYPE.cvterm_id = CR.type_id
    WHERE TYPE.name = 'belongs_to' AND  CR.object_id = :cvterm_id";
  $projects = array();
  $result = chado_query($sql_projects,  array(':cvterm_id' => $cvterm_id));
  while ($p = $result->fetchObject()) {
    $projects[] =
    array(
      'project_id' => $p->project_id,
      'name' => $p->name,
      'type' => $p->type
    );
  }
  $trait->projects = $projects;
  
  // Get pubs
  $sql_pubs = "
          SELECT
            DISTINCT P.pub_id, pyear, P.uniquename
          FROM chado.pub P
          INNER JOIN chado.cvterm V ON V.cvterm_id = P.type_id
          INNER JOIN chado.feature_pub FP ON FP.pub_id = P.pub_id
          INNER JOIN chado.feature_cvterm FV ON FV.feature_id = FP.feature_id
          WHERE FV.cvterm_id =  :cvterm_id
          ORDER BY pyear DESC
    ";
  $pubs = array();
  $result = chado_query($sql_pubs,  array(':cvterm_id' => $cvterm_id));
  while ($p = $result->fetchObject()) {
    $pubs[] =
    array(
      'pub_id' => $p->pub_id,
      'pyear' => $p->pyear,
      'uniquename' => $p->uniquename
    );
  }
  $trait->pubs = $pubs;
  return $trait;
}
